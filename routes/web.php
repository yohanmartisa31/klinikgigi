 <?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', 'AuthController@index')->name('login');
Route::get('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@registerUser');
Route::get('/logout', 'AuthController@logout');
// Route::get('/patient', function () {
//     return view('patient/home');
// });

Route::group(['middleware'=> 'auth'], function(){
	Route::get('/', function () {
	    return view('index');
	});
});
Route::group(['middleware'=> ['auth', 'CheckRole:fo']], function(){
	Route::get('/patient', 'PatientController@index');
	Route::get('/layanan', 'LayananController@index');
	Route::get('/layanan_schedule', 'LayananController@show');
	Route::post('/layanan_schedule/search', 'LayananController@search');
	Route::get('/layanan_schedule/search/{id_patient}', 'LayananController@regis');
	Route::post('/layanan_schedule_category', 'LayananController@category');
	Route::post('/layanan_schedule_save', 'LayananController@store');

	Route::post('/patient', 'PatientController@store');
	Route::delete('/patient/{id_patient}', 'PatientController@destroy');
	Route::get('/patient/{patient}/edit', 'PatientController@edit');
	Route::patch('/patient/{id_patient}', 'PatientController@update');
	Route::get('/patient/register', 'PatientController@create');

	Route::get('/patient/{patient}/registerservice', 'ServiceController@index');
	Route::post('/patient/{patient}/createsession', 'ServiceController@createsession');
	Route::post('/patient/registerservice', 'ServiceController@store');
	Route::get('/patient/printform', 'ServiceController@show');
	Route::get('/patient/cetak_pdf', 'ServiceController@cetak_pdf');
	Route::get('/payment', 'PaymentController@index');
	Route::post('/payment_update', 'PaymentController@update');
	Route::post('/payment', 'PaymentController@search');
	Route::post('/payment/save', 'PaymentController@save');
}); 
Route::group(['middleware'=> ['auth', 'CheckRole:admin']], function(){
	Route::get('/doctor', 'DoctorController@index');
	Route::get('/practice', 'PracticeController@index');
	Route::get('/employees', 'EmployeesController@index');
	Route::get('/schedule_doctor', 'ScheduleDoctorController@index');
	Route::get('/report', 'ServiceController@index');
	Route::post('/report', 'ServiceController@show');
	Route::post('/reportklinik', 'ServiceController@showklinik');
	Route::get('/excel', 'ServiceController@export_excel');
	Route::get('/pdf', 'ServiceController@cetak_pdf');
	Route::get('/excelklinik', 'ServiceController@export_excelklinik');
	Route::get('/pdfklinik', 'ServiceController@cetak_pdfklinik');
	
	Route::post('/doctor', 'DoctorController@store');
	Route::post('/practice', 'PracticeController@store');
	Route::post('/employees', 'EmployeesController@store');
	Route::post('/schedule_doctor_all', 'ScheduleDoctorController@show');
	Route::post('/schedule_doctor_save', 'ScheduleDoctorController@store');
	Route::post('/schedule_doctor_room', 'ScheduleDoctorController@room');

	Route::get('/doctor/register', 'DoctorController@create');
	Route::get('/practice/register', 'PracticeController@create');
	Route::get('/employees/register', 'EmployeesController@create');
	Route::delete('/doctor/{id_doctor}', 'DoctorController@destroy');
	Route::delete('/practice/{id_practice}', 'PracticeController@destroy');
	Route::delete('/employees/{id_employees}', 'EmployeesController@destroy');
	
	Route::get('/doctor/{doctor}/edit', 'DoctorController@edit');
	Route::get('/doctor/{doctor}/reset', 'DoctorController@reset');
	Route::get('/employees/{employees}/edit', 'EmployeesController@edit');
	Route::get('/employees/{employees}/reset', 'EmployeesController@reset');
	Route::get('/practice/{practice}/edit', 'PracticeController@edit');
	Route::patch('/doctor/{id_doctor}', 'DoctorController@update');
	Route::patch('/employees/{id_employees}', 'EmployeesController@update');
	Route::patch('/practice/{id_practice}', 'PracticeController@update');
});

Route::group(['middleware'=> ['auth', 'CheckRole:dokter']], function(){
	Route::get('/dashboarddoctor', 'DashboardDoctorController@index');
	Route::post('/dashboarddoctor', 'DashboardDoctorController@store');
	Route::get('/dashboarddoctor/{task}/form_patient', 'DashboardDoctorController@edit');
});	
