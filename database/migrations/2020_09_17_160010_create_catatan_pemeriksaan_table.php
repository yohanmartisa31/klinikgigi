<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatatanPemeriksaanTable extends Migration
{
    public function up()
    {

        Schema::create('catatan_pemeriksaan', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->bigInteger('id_service_register')->unsigned();		
		$table->foreign('id_service_register')->references('id')->on('service_register')->onDelete('cascade');
		$table->bigInteger('id_employee')->unsigned();
		$table->foreign('id_employee')->references('id')->on('employees');
		$table->bigInteger('id_doctor')->unsigned();		
		$table->foreign('id_doctor')->references('id')->on('doctors');
		$table->string('id_jenis_pelayanan',50);
		$table->string('nama_dokter',50);
		$table->string('nama_pasien',50);
		$table->string('nama_perawat',50);
		$table->string('nama_jenis_pelayanan',50);
		$table->string('price');
		$table->string('diagnosa');
		$table->string('catatan');
		$table->string('complaint');
		$table->date('tanggal')->nullable();
		$table->integer('no_pendaftaran');
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();		               

        });
    }

    public function down()
    {
        Schema::dropIfExists('catatan_pemeriksaan');
    }
}