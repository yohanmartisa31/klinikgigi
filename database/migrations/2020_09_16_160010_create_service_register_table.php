<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceRegisterTable extends Migration
{
    public function up()
    {
        Schema::create('service_register', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->integer('no_pendaftaran');
		$table->string('complaint');
		$table->bigInteger('patient_id')->unsigned();
		$table->foreign('patient_id')->references('id')->on('patients');
		$table->bigInteger('id_doctor')->unsigned();
		$table->foreign('id_doctor')->references('id')->on('doctors');
		$table->enum('cek',['1','0']);
		$table->date('tanggal')->nullable();
        $table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();        

        });
    }

    public function down()
    {
        Schema::dropIfExists('service_register');
    }
}