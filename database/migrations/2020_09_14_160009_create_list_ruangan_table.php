<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListruanganTable extends Migration
{
    public function up()
    {
        Schema::create('listruangan', function (Blueprint $table) {

		$table->integer('id',10)->unsigned();
		$table->string('nama',100);
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('listruangan');
    }
}