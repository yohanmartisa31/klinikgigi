<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHariTable extends Migration
{
    public function up()
    {
        Schema::create('hari', function (Blueprint $table) {

		$table->integer('id',10)->unsigned();
		$table->string('kode_hari');
		$table->string('nama_hari');
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();                

        });
    }

    public function down()
    {
        Schema::dropIfExists('hari');
    }
}