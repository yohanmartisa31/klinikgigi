<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->string('name');
		$table->string('place_of_birth');
		$table->date('date_of_birth');
		$table->string('gender');
		$table->string('address');
		$table->string('religion');
		$table->string('job');
		$table->string('citizenship');
		$table->char('number',255);
		$table->char('identity_number',255);
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();

        });
    }

    public function down()
    {
        Schema::dropIfExists('patients');
    }
}