<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthsTable extends Migration
{
    public function up()
    {
        Schema::create('auths', function (Blueprint $table) {

		$table->integer('id',10)->unsigned();
		$table->string('username',100);
		$table->string('password');
		$table->string('roll');
		$table->string('remember_token',100);
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();                   

        });
    }

    public function down()
    {
        Schema::dropIfExists('auths');
    }
}