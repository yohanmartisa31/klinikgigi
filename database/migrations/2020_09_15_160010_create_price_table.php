<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceTable extends Migration
{
    public function up()
    {
        Schema::create('price', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->string('name_practice');
		$table->string('category_doctor');
		$table->string('price');
		$table->date('tanggal_update_harga')->nullable();
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();            

        });
    }

    public function down()
    {
        Schema::dropIfExists('price');
    }
}