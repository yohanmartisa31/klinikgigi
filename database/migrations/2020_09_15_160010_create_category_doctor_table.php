<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryDoctorTable extends Migration
{
    public function up()
    {
        Schema::create('category_doctor', function (Blueprint $table) {

		$table->integer('id',10)->unsigned();
		$table->string('nama');
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();            

        });
    }

    public function down()
    {
        Schema::dropIfExists('category_doctor');
    }
}