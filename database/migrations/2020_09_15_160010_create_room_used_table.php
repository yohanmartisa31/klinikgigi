<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomUsedTable extends Migration
{
    public function up()
    {
        Schema::create('room_used', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->string('ruangan',100);
		$table->integer('hari')->unsigned();
        $table->foreign('hari')->references('id')->on('hari');
		$table->time('mulai');
		$table->time('selesai');
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();            

        });
    }

    public function down()
    {
        Schema::dropIfExists('room_used');
    }
}