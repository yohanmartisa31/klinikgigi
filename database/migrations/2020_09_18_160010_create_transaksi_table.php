<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiTable extends Migration
{
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->bigInteger('id_catatan_pemeriksaan')->unsigned();
		$table->foreign('id_catatan_pemeriksaan')->references('id')->on('catatan_pemeriksaan')->onDelete('cascade');
		$table->bigInteger('id_karyawan')->unsigned();
		$table->foreign('id_karyawan')->references('id')->on('employees');
		$table->date('tanggal')->nullable();
		$table->string('nama_pasien',50);		
		$table->string('nama_jenis_pelayanan');				
		$table->string('harga');
		$table->string('nama_front_office',50);
		$table->enum('kategori_transaksi',['pelayanan','produk','admin']);
		$table->enum('cara_pembayaran',['tunai','transfer(BCA)','DebitPermata','DebitBankLain','CCAllBank']);
		$table->string('nama_perawat',50);
		$table->string('produk_tambahan',50);
		$table->enum('status',['1','0']);
        $table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();                
        });
    }

    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}