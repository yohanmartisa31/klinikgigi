<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRuanganTable extends Migration
{
    public function up()
    {
        Schema::create('ruangan', function (Blueprint $table) {

		$table->bigInteger('id',10)->unsigned();
		$table->bigInteger('doctor')->unsigned();
        $table->foreign('doctor')->references('id')->on('doctors');        
		$table->bigInteger('ruangan')->unsigned();        
        $table->foreign('ruangan')->references('id')->on('room_used');
		$table->enum('ready',['1','0']);
        $table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();            

        });
    }

    public function down()
    {
        Schema::dropIfExists('ruangan');
    }
}