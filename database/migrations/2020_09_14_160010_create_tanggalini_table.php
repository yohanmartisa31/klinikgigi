<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanggaliniTable extends Migration
{
    public function up()
    {
        Schema::create('tanggalini', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->date('tanggal')->nullable();
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();

        });
    }

    public function down()
    {
        Schema::dropIfExists('tanggalini');
    }
}