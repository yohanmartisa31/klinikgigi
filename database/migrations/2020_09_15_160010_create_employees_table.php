<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->string('name');
		$table->enum('category',['fo','perawat','officeboy','admin']);
		$table->string('gender');
		$table->string('address');
		$table->char('number',255);
		$table->char('identity_number',255);
		$table->string('username',10);
		$table->string('password');
		$table->dateTime('pass_reset_date')->nullable;
		$table->string('reset_by',10);
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();         

        });
    }

    public function down()
    {
        Schema::dropIfExists('employees');
    }
}