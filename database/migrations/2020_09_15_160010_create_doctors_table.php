<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {

		$table->bigInteger('id',20)->unsigned();
		$table->string('name');
		$table->string('category');
		$table->string('place_of_birth');
		$table->date('date_of_birth');
		$table->string('gender');
		$table->string('address');
		$table->string('religion');
		$table->char('number',255);
		$table->char('identity_number',255);
		$table->string('username',10);
		$table->string('password');
		$table->dateTime('pass_reset_date')->nullable;
		$table->string('reset_by',10);
		$table->dateTime('created_at')->nullable();
        $table->dateTime('updated_at')->nullable();		       

        });
    }

    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}