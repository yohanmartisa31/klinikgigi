@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA MASTER (JENIS PELAYANAN)</h3>			    
			    <a href="{{ url('/practice/register') }}" class="btn btn-success"><i class="lnr lnr-user"></i><span> Register Jenis Pelayanan</span></a>			

		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form method="post" action="/practice">
			  @csrf			  
			  <div class="form-group">
			    <label for="category_doctor">Kategory Doktor</label>
			    <select type="text" class="form-control @error('category_doctor') is-invalid @enderror" id="category_doctor"  name="category_doctor">
				  @error('category_doctor')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
				  <option value="none" selected disabled hidden> 
          			Select category doctor 
      			  </option> 
      			  	  <option value="umum">umum</option>
					  <option value="Spesialis Kedokteran Gigi Anak (Sp. KGA)">Spesialis Kedokteran Gigi Anak (Sp. KGA)</option>
					  <option value="Spesialis Konservasi Gigi (Sp. KG)">Spesialis Konservasi Gigi (Sp. KG)</option>
					  <option value="Spesialis Penyakit Mulut (Sp. PM)">Spesialis Penyakit Mulut (Sp. PM)</option>
					  <option value="Spesialis Ortodonsia (Sp. Ort)">Spesialis Ortodonsia (Sp. Ort)</option>
					  <option value="Spesialis Periodonsia (Sp. Perio)">Spesialis Periodonsia (Sp. Perio)</option>
					  <option value="Spesialis Prostodonsia (Sp. Pros)">Spesialis Prostodonsia (Sp. Pros)</option>
					  <option value="Spesialis Radiologi Kedokteran Gigi (Sp. RKG)">Spesialis Radiologi Kedokteran Gigi (Sp. RKG)</option>
					  <option value="Spesialis Bedah Mulut (Sp. BM)">Spesialis Bedah Mulut (Sp. BM)</option>			
				</select>
			  </div>
			  <div class="form-group">
			    <label for="name">Nama jenis pelayanan</label>
			    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="input patient name" name="name" value="{{ old('name')}}">
				  @error('name')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			  
			
			  <div class="form-group col-5">
			    <label for="price">Harga</label>
			    Rp.<input type="number" step="1000" class="form-control @error('price') is-invalid @enderror" id="price" placeholder="input patient contact price" name="price"  value="{{ old('price')}}">
			    @error('price')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>  			
			  <button type="submit" class="btn btn-primary">Register</button>
			  <a href="{{ url('/patient') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
