@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA MASTER (JENIS PELAYANAN)</h3>			    
			    <a href="{{ url('/practice/register') }}" class="btn btn-success"><i class="lnr lnr-user"></i><span> Register Jenis Pelayanan</span></a>			

		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body"style="margin-top:5%;">
		  <div class="canvas-wrapper" height="1000" style="overflow-x:auto;">
			  <table class="table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">
				  	<th scope="col">No</th>				  	
				  	<th scope="col">Kategory Dokter</th>
				  	<th scope="col">Nama praktik</th>
				  	<th scope="col">Harga</th>				  	
				  	<th scope="col">Data Update</th>				  	
				  	<th scope="col">Aksi</th>
				  </tr>
				</thead>
				<tbody>
				  @foreach( $practice as $practice ) 
				  <tr>
				  	<th scope="row">{{ $loop->iteration }}</th>
				  	<td class="col-sm-2">{{ $practice->category_doctor }}</td>				  	
				  	<td  class="col-sm-3">{{ $practice->name_practice }}</td>
				  	<td  class="col-sm-2">Rp.{{ $practice->getPrice->price }}</td>
				  	<td  class="col-sm-2">{{ $practice->getPrice->tanggal_update_harga }}</td>
				  	<td class="col-sm-2">
		            <a href="/practice/{{ $practice->id }}/edit" class="btn btn-primary" style="width:100%;margin-bottom: 5px;">Update harga</a>
				  	<form action="/practice/{{ $practice->id }}" method="post" class="d-inline">
				 	@method('delete')
    				@csrf
				  	  <button type="submit" class="btn btn-danger" style="width:100%;">Delete</button>
				  	</form>		          
				  	</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
		  </div>			
		</div>
	  </div>
	</div>
  </div>
</div>
  <!--/.row-->
@endsection
