@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
				<h3 class="panel-title">Update JENIS PELAYANAN</h3>
				<h4 calss="panel-title">Kategory Dokter : {{$practice->category_doctor}}</h4>
				<h4 calss="panel-title">Nama Pelayanan : {{$practice->name_practice}}</h4>
		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>

		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/practice/{{ $practice->id}}" method="post" class="d-inline">			  
			 	@method('patch')
    			@csrf		
    		  <input type = "hidden" name="name_practice" value="{{$practice->name_practice}}">
    		  <input type = "hidden" name="category_doctor" value="{{$practice->category_doctor}}">
			  <div class="form-group">
			    <label for="price">price</label>
			    Rp.<input type="number" step="1000" class="form-control @error('price') is-invalid @enderror" id="price" placeholder="Edit practice price" name="price">
				  @error('price')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>			    			  
			  <button type="submit" class="btn btn-primary">edit</button>
			  <a href="{{ url('/doctor') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
