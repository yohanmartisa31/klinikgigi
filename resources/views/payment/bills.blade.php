 @extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
	  <div class="container-fluid">	
		<div class="panel panel-headline">
		    <div class="panel-heading">
			  <div class="row">
				
					<h3 class="panel-title">PEMBAYARAN</h3>			    			    
					<h4>tanggal: {{ $date }}</h4>
			  </div>
			  @if (session('status'))
			    <div class="alert alert-warning">
			      {{ session('status') }}
			    </div>
			  @endif
			</div>

			<div class="panel-body">
			  <div class="canvas-wrapper" height="200">			
				<form method="post" action="/payment">
				  @csrf			  
				  <div class="form-group col-7">
				    <label for="no_pendaftaran"> No Antrian Patient</label>
				    <input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" placeholder="input no antrian" value="{{ old('no_pendaftaran')}}">
				    @error('id')
		              <div class="invalid-feedback">
					{{ $message }}
					</div>
					@enderror			
				</div>
					<button type="submit" class="btn btn-primary">Bayar</button>
					<a href="{{ url('/payment') }}" class="btn btn-secondary">Back</a>
				</form>
			  </div>
			</div>
		</div>
		<div class="panel panel-headline">
			<div class="panel-body">
			  <div class="canvas-wrapper" height="200">			
				
				<h2 class="text-center metric">Struk pembayaran </h2>			
				<div class="metric">
					<h3>Form pembayaran</h3>			  
				    <h4>No Pendaftaran :{{$catatan_pemeriksaan->id}}</h4>	  
				    <h4>Name Pasien :{{$catatan_pemeriksaan->nama_pasien}}</h4>
				    <h4>Nama Dokter :{{$catatan_pemeriksaan->nama_dokter}}</h4>			  				    
				    <h4 class="col-2">Keluhan :{{$catatan_pemeriksaan->complaint}}</h4>			    
				  	<h4 class="col-2">Diagnosa Dokter :{{$catatan_pemeriksaan->diagnosa}}</h4>			  
				    <h4 class="col-2">catatan Dokter :{{$catatan_pemeriksaan->catatan}}</h4>			  			  
				<div class="metric">	
					  <div style ="display: flex;    justify-content:space-around;">
					    <h3 class="col-6">Jenis Pelayanan </h3>
					    <h3 class="col-6">Harga </h3>
					  </div>
					  <div style ="display: flex;    justify-content:space-around;">			    
					    	<ul>
					    	@foreach($jenispelayanan as $temp)
					    	<li>{{$temp->name_practice}}</li>
					    	@endforeach
					    	</ul>
					    	<ul>
					    	@foreach($harga as $temp)
					    	<li  style="">: Rp.{{$temp->getPrice->price}}</li>
					    	@endforeach
					    </ul>
					  </div>
				</div>    
					 <?php $total = $catatan_pemeriksaan->price+$tambahan; ?>			   			
					 <ul>
					 	<li>{{$produk}}</li>
					 </ul>

					<h4>Total :Rp.{{$total}}</h4>
					<button type="button" class="btn btn-info" data-toggle="modal" data-target=".create">Biaya tambahan</button>
				 
				  	<!--  -->
				<!--   <button type="submit" class="btn btn-primary">register service</button> -->
				  <form action="/payment/save" method="post" class="d-inline">	 	
	    				@csrf
	    	      		<div class="form-group col-7">
						    <label for="metode"> pilih metode pembayaran</label>
						    <input class="hidden" name="total" value="{{ $total }}"required>
						    <select type="text" class="form-control @error('metode') is-invalid @enderror" id="metode"  name="metode"required>
						    @error('metode')
							    <div class="invalid-feedback">
							      {{ $message }}
							    </div>
						    @enderror
							    <option value="none" selected disabled hidden> 
							    	Select method payment available 
								</option> 				      			  							
							  	<option value="tunai">tunai </option>						
							  	<option value="transfer(BCA)">transfer (BCA) </option>						
							  	<option value="DebitPermata"> Debit Permata </option>						
							  	<option value="DebitBankLain"> Debit Bank Lain </option>				
							  	<option value="CCAllBank"> CC All Bank </option>											  			
						  </select>  
						</div>

					  	  <button type="submit" class="btn btn-success">Lunas</button>
				  </form>		          
				  <a href="{{ url('/payment') }}" class="btn btn-secondary">Back</a>
				</div>
			  </div>			
			</div>
	  	</div>
	</div>
</div>
  <!--/.row-->
<div class="modal fade create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/payment_update" method="post">

                <div class="modal-header" style="background:#00a65a;color:#FFFFFF">
                    <h4 class="modal-title">Biaya tambahan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="input1">Produk *</label>
                        <input name="nama" class="form-control" id="input1" placeholder="Masukkan Tahun Akademik" type="text" maxlength="50" required>
                    </div>
                </div>
                <div class="form-group">
                        <label for="input2">Harga *</label>
                        <input name="harga" class="form-control" id="input2" placeholder="Masukkan harga Tambahan" type="number" maxlength="50" required>
                    </div>
                
                <div class="modal-footer">
                  <input type="hidden" name="_token" value="{{csrf_token()}}" />                    
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>   
            </form>
        </div>
    </div>
</div>
@endsection