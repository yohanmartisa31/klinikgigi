 @extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">PEMBAYARAN</h3>			    			    
				<h4>Daftar pasien yang telah melakukan pemeriksaan</h4>
				<h4>Hari: {{ $day->nama_hari }}</h4>
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form method="post" action="/payment">
			  @csrf			  
			  <div class="form-group col-7">
			    <label for="no_pendaftaran"> No Antrian Patient</label>
			    <input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" placeholder="input no antrian" value="{{ old('no_pendaftaran')}}">
			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>
				<button type="submit" class="btn btn-primary">Bayar</button>
				<a href="{{ url('/') }}" class="btn btn-secondary">Back</a>
			</form>

			<table class="table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">				  	
				  	<th scope="col">No.Pendaftaran</th>
				  	
				  	<th scope="col">Nama Pasien</th>
				  	<th scope="col">Komplain</th>
				  	<th scope="col">Dokter pemeriksa</th>						  	
				  	<th scope="col">Jadwal</th>						  					  				  
				  </tr>
				</thead>
				<tbody>
				  @foreach( $listpatient as $task )				  
				  <tr>
				  	<td>{{ $task->no_pendaftaran }}</td>				  	
				  	<td  class="col-sm-3">{{ $task->getPatient->name }}</td>
				  	<td  class="col-sm-3">{{ $task->complaint }}</td>				  					  	
				  	<td  class="col-sm-3">{{ $task->getDoctor->name }}</td>				  					  		
					
				  	<td  class="col-sm-3"><label style="color:blue;">Telah Diperiksa</label></td>				  						  	
				  </tr>				  
				  @endforeach
				</tbody>
			  </table>
	  	  </div>
	  	</div>

	  </div>

	</div>
  </div><!--/.row-->
@endsection
