<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | Klinik Gigiku</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/font-awesome.min.css')}}">        
        <link href="{{asset('css/main.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('vendor/timepicker/bootstrap-timepicker.min.css')}}">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">  
        <link rel="icon" type="image/png" sizes="96x96" href="img/gigi.png">        

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->        
    </head>
    <body>
        <div id="wrapper">
            <!-- NAVBAR -->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="brand">             
                    <a href="index.html"><img src="{{asset('img/gigi.png')}}" alt="gigiku" class="img-responsive logo" style="width:100px;"></a>
                    <h1>Gigiku  </h1>               
                </div>
                <div class="container-fluid">
                    <div class="navbar-btn">
                        <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                    </div>
                    <!-- <form class="navbar-form navbar-left">
                        <div class="input-group">
                            <input type="text" value="" class="form-control" placeholder="Search dashboard...">
                            <span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
                         </div>
                    </form> -->
                    <!-- <div class="navbar-btn navbar-btn-right">
                        <a class="btn btn-success update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
                    </div> -->
                    <div id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right">                        
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{{asset('img/user1.jpg')}}" class="img-circle" alt="Avatar"> <span>{{session('username')}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                                <ul class="dropdown-menu">
                                    
                                    <li><a href="{{url('/logout')}}"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                                </ul>
                            </li>
                            <!-- <li>
                                <a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        <div id="sidebar-nav" class="sidebar">
            <div class="sidebar-scroll">
                <nav>
                    <ul class="nav">
                        <li><a href="{{ url ('/')}}" class="{{ (request()->is('/')) ? 'active' : '' }}"><i class="lnr lnr-home"></i> <span>Dashboard</span></a></li>
                        @if(auth()->user()->roll == 'admin')
                        <li>
                        <span style="margin-left: 10px;">Data</span>
                            <a href="#subPages" data-toggle="collapse" class="collapsed"><i class="lnr lnr-menu"></i> <span>Data Master</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                            <div id="subPages" class="collapse ">
                                <ul class="nav">
                                    <li><a href="{{ url('/employees') }}" class="{{ (request()->is('employees')) ? 'active' : '' }}"><i class="lnr lnr-users"></i> <span>Karyawan</span></a></li>
                                    <li><a href="{{ url('/doctor') }}" class="{{ (request()->is('doctor')) ? 'active' : '' }}"><i class="lnr lnr-users"></i> <span>Dokter</span></a></li>
                                    <li><a href="{{ url('/practice') }}" class="{{ (request()->is('practice')) ? 'active' : '' }}"><i class="lnr lnr-list"></i> <span>Jenis pelayanan</span></a></li>                                       
                                </ul>
                            </div>
                        </li>
                        <span style="margin-left: 10px;">Laporan</span>
                        <li><a href="{{ url('/report')}}" class=""><i class="{{ (request()->is('report')) ? 'active' : '' }}lnr lnr-chart-bars"></i> <span>Laporan keuangan</span></a></li>
                        @endif
                        @if(auth()->user()->roll == 'fo')
                        <span style="margin-left: 10px;">Cek Medis</span>
                        <li><a href="{{ url('/patient') }}" class="{{ (request()->is('patient')) ? 'active' : '' }}"><i class="lnr lnr-dice"></i> <span>Data Pasien</span></a></li>
                        <li><a href="{{url('/layanan_schedule')}}" class="{{ (request()->is('layanan_schedule')) ? 'active' : '' }}"><i class="lnr lnr-user"></i> <span>Pendaftaran</span></a></li>
                        <li><a href="{{url('/payment')}}" class=""><i class="{{ (request()->is('payment')) ? 'active' : '' }}"></i> <span>Pembayaran</span></a></li>
                        @endif
                        @if(auth()->user()->roll == 'dokter')
                        <span style="margin-left: 10px;">Pemeriksaan dokter</span>
                        <li><a href="{{ url('/dashboarddoctor')}}" class="{{ (request()->is('dashboarddoctor')) ? 'active' : '' }} "><i class="lnr lnr-dice"></i> <span>pemeriksaan</span></a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>


        