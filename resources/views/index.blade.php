@extends('template')

@section('content')
  <div class="main">
	@if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<!-- OVERVIEW -->
					<div class="panel panel-headline">
						<div class="panel-heading">
							<h3 class="panel-title">Selamat Datang di Web Klinik gigi!</h3>													
							<p class="panel-subtitle"></p>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="metric">
										<span class="icon"><i class="fa fa-user"></i></span>
										<p>
											<span class="number">Username</span>
											<span class="title">{{ session('username') }}</span>
										</p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="metric">
										<span class="icon"><i class="fa fa-calendar"></i></span>
										<p>
											<span class="number">Tanggal</span>
											<span class="title">{{ session('date') }}</span>
										</p>
									</div>
								</div>
								<div class="col-md-4">
									<div class="metric">
										<span class="icon"><i class="fa fa-users"></i></span>
										<p>
											<span class="number">Pasien Terdaftar</span>
											<span class="title">Total {{ session('patient') }}</span>
										</p>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<!-- END OVERVIEW -->
					
					
			</div>
			<!-- END MAIN CONTENT -->
		</div>
@endsection
