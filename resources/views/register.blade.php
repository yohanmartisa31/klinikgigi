<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lumino - Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/datepicker3.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
</head>
<body>
<div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
    
                <form action="{{ url('/register') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <!-- <div class="form-group">
                        <label for="alamat">Password Confirmation:</label>
                        <input type="password" class="form-control" id="confirmation" name="confirmation">
                    </div> -->
                    <div class="form-group">
                        <label for="roll">roll</label>
                        <input type="text"  class="form-control" id="roll" name="roll">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Register</button>
                        <a href="{{ url('/login') }}" class="btn btn-secondary">back</a>
                    </div>
                </form>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->    
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
