<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Login | klinik gigiku</title>
	<!-- css -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
<!-- 	<link href="css/styles.css" rel="stylesheet"> -->
	<link href="css/main.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">	
	<link rel="icon" type="image/png" sizes="96x96" href="img/gigi.png">
</head>
<body>
	<!-- <div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				@if (session('status'))
				    <div class="alert alert-success">
				      {{ session('status') }}
				    </div>
			  	@endif
<<<<<<< HEAD
				<div class="panel-heading">LOGIN</div>
=======
				<div class="panel-heading">ini Login (setelah deploy)</div>
>>>>>>> 297ddb589815e9ed34672f9b06e739f90dd23ae8
		 		<div class="panel-body">
					<form role="form" method="post" action="/login">
					@csrf	
						<fieldset>
							<div class="form-group">
								<input class="form-control @error('username') is-invalid @enderror" placeholder="Username" name="username" type="text" value="{{ old('username')}}">
								@error('username')
								    <div class="invalid-feedback .text-danger">
								      {{ $message }}
								    </div>
							    @enderror
							</div>
							<div class="form-group">
								<input class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" type="password" value="">
								@error('password')
								    <div class="invalid-feedback .text-danger">
								      {{ $message }}
								    </div>
							    @enderror
							</div>	
							<button type="submit" class="btn btn-primary">Login</button>	
                            <a href="{{ url('/register') }}" class="btn btn-secondary">Register</a>
			
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div> -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><img src="img/gigi.png" alt="gigiku Logo" class="img_logo"></div>
								<p class="lead">Login to your klinik</p>
								@if (session('status'))
								    <div class="alert alert-danger">
				      				{{ session('status') }}
								    </div>
			  					@endif
							</div>							
							<form role="form-auth-small" method="post" action="/login">
							@csrf	
								<fieldset>
								<div class="form-group">
									<span style="float:left">username</span>
									<label for="username" class="control-label sr-only">username</label>
									<input class="form-control @error('username') is-invalid @enderror" placeholder="Username" name="username" type="text" value="{{ old('username')}}">
									@error('username')
								    <div class="invalid-feedback .text-danger">
								      {{ $message }}
								    </div>
							    	@enderror
								</div>
								<div class="form-group">
									<span style="float:left">password</span>
									<label for="signin-password" class="control-label sr-only">Password</label>
									<input class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" type="password" value="">
									@error('password')
								        <div class="invalid-feedback .text-danger">
								      {{ $message }}
								        </div>
							        @enderror
								</div>								
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								</fieldset>
							</form>
						</div>
					</div>
					<!-- <div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Free Bootstrap dashboard template</h1>
							<p>by The Develovers</p>
						</div>
					</div> -->
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	

<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>
