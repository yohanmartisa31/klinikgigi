@extends('template')

@section('content')
<div class="main" style="min-height: 0;">
  <div class="main-content">
	<div class="container-fluid">	
      <div class="panel panel-headline col-md-5">
	    <div class="panel-heading">
		  <div class="row">			
		    <h2><b>FORM PEMERIKSAAN</b></h2>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/dashboarddoctor" method="post" class="d-inline">			  
    		  @csrf			  
			  <h4>No Pendaftaran: {{ $task->no_pendaftaran }}</h4>			  
			  <h4>Keluhan: {{ $task->complaint }}</h4>
			  <div class="form-group">
			    <label for="diagnosa">Diagnosa</label>
			    <textarea type="text" class="form-control" id="diagnosa"  name="diagnosa"></textarea>
			  </div> 			  
			  <div class="form-group ">
			  	<label for="practice">Tindakan/Jenis Pelayanan</label>
			  @foreach( $practice as $practice )			  				  	
				<div class="form-check col-12">
				  <input class="form-check-input" type="checkbox" id="practice" name="practice[]" value="{{$practice->id}}">
				  <label class="form-check-label" for="practice" style="margin-left: 2%;" >
					{{$practice->name_practice}}			      				    
				  </label>
				</div>			    
			  @endforeach			  			  	
			  <div class="form-group">
			    <label for="note">Catatan</label>
			    <textarea type="text" class="form-control" id="note"  name="note"></textarea>
			  </div>
			  <div class="form-group">		 	    
			    <input type="hidden" class="form-control" id="patient_id" name="patient_id" value="{{ $task->patient_id }}">
			  </div>			 
			  <div class="form-group">		 	    
			    <input type="hidden" class="form-control" id="complaint" name="complaint" value="{{ $task->complaint }}">
			  </div>
			  <div class="form-group">
			  <label for="perawat">Pilih perawat</label>
				  <select type="text" class="form-control @error('perawat') is-invalid @enderror" id="perawat"  name="perawat">
				    @error('perawat')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
				    @enderror
					    <option value="none" selected disabled hidden> 
					    	Select perawat available 
						</option> 				      			  
					@foreach($employee as $temp)		  		
					@if($temp == null)
						<option value="" disabled hidden>tidak ada list perawat</option>  			 	  
					@else
					  	<option value="{{$temp->id}}">{{$temp->name}} </option>
					@endif
					@endforeach
				  </select>  
			  </div>
			  <button type="submit" class="btn btn-primary">submit</button>
			  <a href="{{ url('/dashboarddoctor') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>		
	  </div>
	</div>			
	<div class="container-fluid">	
	  <div class="panel panel-headline col-md-6"style="margin-left: 50px;">
	    <div class="panel-heading">
		  <div class="row">			
		    <h2><b>CATATAN PEMERIKSAAN</b></h2>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
		  	<h4>Nama Pasien: {{ $task->getPatient->name }}</h4>
		  	<h4>Id Pasien: {{ $task->patient_id }}</h4>			  
		  	<table class="table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">				  	
				  	<th scope="col-1">#</th>
				  	<th scope="col-1">Tanggal</th>
				  	<th scope="col-1">Nama Dokter</th> 	
				  	<th scope="col-1">Jenis pelayanan</th>
				  </tr>
				</thead>
				<tbody>				  
				  <tr>
				  	<td class="col-sm-1"></td>				  	
				  	<td class="col-sm-1"></td>				  					  					  		
				  	<td class="col-sm-1"></td>				  			            	
				  	<td class="col-sm-1"></td>				  			            	
				  </tr>				  
				</tbody>
			</table>
	</div>				
  </div>
</div>
@endsection
