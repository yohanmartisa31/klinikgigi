@extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h2><b>LIST PASIEN</b></h2>			    			    

		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		  <h4>Kode dokter: {{$doctor->id}}</h4>
		  <h4>Nama dokter: {{$doctor->name}}</h4>
		  <h4>Tanggal: {{$date}}</h4>
		</div>
		<div class="panel-body"style="margin-top:5%;">
		  <div class="canvas-wrapper" height="1000">
			  <table class="table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">				  	
				  	<th scope="col-1">No.Pendaftaran</th>
				  	<th scope="col-4">Nama Pasien</th> 	
				  	<th scope="col-1">Action</th>
				  </tr>
				</thead>
				<tbody>
				  @foreach( $listpatient as $task )
				  @if($task == null)
				   
				  @else
				  <tr>
				  	<td class="col-sm-1">{{ $task->no_pendaftaran }}</td>				  	
				  	<td  class="col-sm-4">{{ $task->getPatient->name }}</td>				  					 				  					  		
				  	<td class="col-sm-1">
		            	<a href="/dashboarddoctor/{{ $task->id }}/form_patient" class="btn btn-primary">Periksa</a>
		        	</td>
				  </tr>
				  @endif
				  @endforeach
				</tbody>
			  </table>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
