@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
	  <div class="container-fluid">
					<!-- OVERVIEW -->
	    <div class="panel panel-headline">
		    <div class="panel-heading">
			  <div class="row">				
					<h3 class="panel-title">EDIT DATA KARYAWAN </h3>			    
					<h3 class="panel-title" style="color:blue;">{{$employees->name}}</h3>
			  </div>
			</div>  	
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/employees/{{ $employees->id}}" method="post" class="d-inline">			  
			 	@method('patch')
    			@csrf			  
    			<div class="form-group">
				    <label for="name">Nama</label>
				    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="input patient name" name="name" value="{{ $employees->name }}">
					  @error('name')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="category">Kategory Karyawan</label>
				    <select type="text" class="form-control @error('category') is-invalid @enderror" id="category"  name="category">					  
					  <option value="{{ $employees->category }}" selected hidden> 
	          			{{ $employees->category }}
	      			  </option> 
					  <option value="fo">Front office</option>
					  <option value="admin">admin</option>				  
					  <option value="perawat">perawat</option>				  
					  <option value="officeboy">office boy</option>				  
					</select>
					@error('category')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  								  								
				  <div class="form-group">
				    <label for="gender">Jenis Kelamin</label>
				    <select type="text" row="3" class="form-control @error('gender') is-invalid @enderror" id="gender" name="gender">				    
					  <option value="{{ $employees->gender }}" selected hidden> 
	          			{{ $employees->gender }}
	      			  </option> 
					  <option value="PRIA">pria</option>
					  <option value="WANITA">wanita</option>
					</select>
					@error('gender')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
				
				  <div class="form-group">
				    <label for="address">Alamat</label>
				    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address"  name="address"  value="{{ $employees->address }}">				    							
					@error('address')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
				  <div class="form-group">
				    <label for="number">No.Telepon</label>
				    <input type="text" class="form-control @error('number') is-invalid @enderror" id="number" placeholder="input patient contact number" name="number"  value="{{ $employees->number}}">
				    @error('number')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="identity_number">No Identitas(KTP/Passport)</label>
				    <input type="text" class="form-control @error('identity_number') is-invalid @enderror" id="identity_number" placeholder="input patient contact identity_number" name="identity_number"  value="{{ $employees->identity_number}}">
				    @error('identity_number')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="username">username</label>
				    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="input username" name="username"  value="{{ $employees->username}}">
				    @error('username')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
			  <button type="submit" class="btn btn-primary">edit</button>
			  <a href="{{ url('/employees') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div>
</div>

  <!--/.row-->
@endsection
