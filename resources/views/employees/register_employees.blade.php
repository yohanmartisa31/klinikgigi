@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
	  <div class="container-fluid">
					<!-- OVERVIEW -->
	    <div class="panel panel-headline">
		    <div class="panel-heading">
			  <div class="row">
				
					<h3 class="panel-title">REGISTRASI EMPLOYEES BARU</h3>			    
				    
				

			  </div>
			</div>  	



			<div class="panel-body"style="margin-top:5%;">

			  <div class="canvas-wrapper" height="600">
				<form method="post" action="/employees">
				  @csrf			  
				  <div class="form-group">
				    <label for="name">Nama</label>
				    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="input patient name" name="name" value="{{ old('name')}}">
					  @error('name')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="category">Kategory Karyawan</label>
				    <select type="text" class="form-control @error('category') is-invalid @enderror" id="category"  name="category">					  
					  <option value="none" selected disabled hidden> 
	          			Select category employee
	      			  </option> 
					  <option value="fo">Front office</option>
					  <option value="admin">admin</option>				  
					  <option value="perawat">perawat</option>				  
					  <option value="officeboy">office boy</option>				  
					</select>
					@error('category')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  								  								
				  <div class="form-group">
				    <label for="gender">Jenis Kelamin</label>
				    <select type="text" row="3" class="form-control @error('gender') is-invalid @enderror" id="gender" name="gender">				    
					  <option value="none" selected disabled hidden> 
	          			Select gender 
	      			  </option> 
					  <option value="PRIA">pria</option>
					  <option value="WANITA">wanita</option>
					</select>
					@error('gender')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
				
				  <div class="form-group">
				    <label for="address">Alamat</label>
				    <textarea type="text" class="form-control @error('address') is-invalid @enderror" id="address"  name="address"  value="{{ old('address')}}">				    		
					</textarea>
					@error('address')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
				  <div class="form-group">
				    <label for="number">No.Telepon</label>
				    <input type="text" class="form-control @error('number') is-invalid @enderror" id="number" placeholder="input patient contact number" name="number"  value="{{ old('number')}}">
				    @error('number')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="identity_number">No Identitas(KTP/Passport)</label>
				    <input type="text" class="form-control @error('identity_number') is-invalid @enderror" id="identity_number" placeholder="input patient contact identity_number" name="identity_number"  value="{{ old('identity_number')}}">
				    @error('identity_number')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>
				  <div class="form-group">
				    <label for="username">username</label>
				    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="input username" name="username"  value="{{ old('username')}}">
				    @error('username')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
				  
                
                
				  <button type="submit" class="btn btn-success">Register</button>
				  <a href="{{ url('/employees') }}" class="btn btn-secondary">Back</a>
				</form>
			  </div>			
		</div>
	  </div>
	</div>
  </div>
</div>
  <!--/.row-->

@endsection
