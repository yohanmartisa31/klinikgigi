@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA MASTER (KARYAWAN)</h3>			    
			    <a href="{{ url('/employees/register') }}" class="btn btn-success"><i class="lnr lnr-user"></i><span> Register Karyawan</span></a>			

		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="main-content">
		<div class="container-fluid">
		<div class="panel-body"style="margin-top:5%;">
		  <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">
			  <table class="table table-bordered table-hover">
				<thead class="thead">
				  <tr class="table-primary">
				  	<th scope="col">No</th>				  	
				  	<th scope="col">Name</th>
				  	<th scope="col">Number</th>
				  	<th scope="col">Category</th>				  	
				  	<th scope="col">Action</th>
				  </tr>
				</thead>
				<tbody>
				  @foreach( $employee as $employee ) 
				  <tr>
				  	<th scope="row">{{ $loop->iteration }}</th>				  	
				  	<td  class="col-sm-3">{{ $employee->name }}</td>
				  	<td  class="col-sm-3">{{ $employee->number }}</td>
				  	<td  class="col-sm-2">{{ $employee->category }}</td>
				  	<td class="col-sm-2">
				  	<a href="/employees/{{ $employee->id }}/reset" class="btn btn-warning " style="width:100%;">Reset</a>	
		            <a href="/employees/{{ $employee->id }}/edit" class="btn btn-primary "style="width:100%;margin-top: 5px;margin-bottom: 5px;">Edit</a>
				  	<form action="/employees/{{ $employee->id }}" method="post" class="">
				 	@method('delete')
    				@csrf
				  	  <button type="submit" class="btn btn-danger"style="width:100%;">Delete</button>
				  	</form>		          
				  	</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
		  </div>			
		</div>
	  </div>
	</div>
</div>
  </div>
</div>
</div>

  <!--/.row-->
@endsection
