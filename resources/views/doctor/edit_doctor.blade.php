@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
	  <div class="container-fluid">
					<!-- OVERVIEW -->
	    <div class="panel panel-headline">
		    <div class="panel-heading">
			  <div class="row">				
					<h3 class="panel-title">EDIT DATA DOKTER </h3>			    
					<h3 class="panel-title" style="color:blue;">{{$doctor->name}}</h3>
			  </div>
			</div>  	
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/doctor/{{ $doctor->id}}" method="post" class="d-inline">			  
			 	@method('patch')
    			@csrf			  

			  <div class="form-group">
			    <label for="name">Nama</label>
			    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Edit doctor name" name="name" value="{{ $doctor->name }}">
				  @error('name')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			  <!--  -->
			  <div class="form-group">
			    <label for="category">Kategory Dokter</label>
			    <select type="text" class="form-control @error('category') is-invalid @enderror" id="category"  name="category" >  
				  <option value="{{$doctor->category}}" selected hidden> 
          			{{$doctor->category}}
      			  </option> 
      			  <option value="umum">umum (Dr. g)</option>
					  <option value="Spesialis Kedokteran Gigi Anak (Sp. KGA)">Spesialis Kedokteran Gigi Anak (Sp. KGA)</option>
					  <option value="Spesialis Konservasi Gigi (Sp. KG)">Spesialis Konservasi Gigi (Sp. KG)</option>
					  <option value="Spesialis Penyakit Mulut (Sp. PM)">Spesialis Penyakit Mulut (Sp. PM)</option>
					  <option value="Spesialis Ortodonsia (Sp. Ort)">Spesialis Ortodonsia (Sp. Ort)</option>
					  <option value="Spesialis Periodonsia (Sp. Perio)">Spesialis Periodonsia (Sp. Perio)</option>
					  <option value="Spesialis Prostodonsia (Sp. Pros)">Spesialis Prostodonsia (Sp. Pros)</option>
					  <option value="Spesialis Radiologi Kedokteran Gigi (Sp. RKG)">Spesialis Radiologi Kedokteran Gigi (Sp. RKG)</option>
					  <option value="Spesialis Bedah Mulut (Sp. BM)">Spesialis Bedah Mulut (Sp. BM)</option>			
				</select>
				  @error('category')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			  <!--  -->
			  <div class="form-group col-7">
			    <label for="place_of_birth">Tempat Lahir</label>
			    <input type="text" class="form-control @error('place_of_birth') is-invalid @enderror" id="place_of_birth" placeholder="edit doctor contact place_of_birth" name="place_of_birth"  value="{{ $doctor->place_of_birth}}">
			    @error('place_of_birth')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
				</input>
			  </div>
			  <div class="form-group col-5">
			    <label for="date_of_birth">tanggal lahir</label>
			    <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" id="date_of_birth" placeholder="edit doctor contact date_of_birth" name="date_of_birth"  value="{{ $doctor->date_of_birth}}">
			    @error('date_of_birth')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>  
			<!--  -->
			
			  <div class="form-group col-6">
			    <label for="gender">Jenis Kelamin</label>
			    <select type="text" row="3" class="form-control @error('gender') is-invalid @enderror" id="gender" placeholder="input doctor contact gender" name="gender">
				  <option value="{{$doctor->gender}}" selected hidden> 
          			{{$doctor->gender}}
      			  </option>  
				  <option value="PRIA">pria</option>
				  <option value="WANITA">wanita</option>
				</select>
			    @error('gender')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror				
			  </div>
			  <!--  -->
			  <div class="form-group col-6">
			    <label for="religion">Agama</label>
			    <select type="text" class="form-control @error('religion') is-invalid @enderror" id="religion" placeholder="input doctor contact religion" name="religion">
				  <option value="{{$doctor->religion}}" selected hidden> 
          			{{$doctor->religion}}
      			  </option>
				  <option value="islam">islam</option>
					  <option value="hindu">hindu</option>
					  <option value="budha">budha</option>
					  <option value="katolik">katolik</option>
					  <option value="protestan">protestan</option>
					  <option value="konghucu">kong hu cu</option>
					  <option value="lain-lain">lain-lain</option>				  
				</select>
			    @error('religion')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			<!--  -->
			  <div class="form-group">
			    <label for="address">Alamat</label>
			    <input type="text" class="form-control @error('address') is-invalid @enderror" id="address"  name="address"  value="{{ $doctor->address }}">				
			    @error('address')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror			
			  </div>

			  <div class="form-group">
			    <label for="number">No.Telepon</label>
			    <input type="text" class="form-control @error('number') is-invalid @enderror" id="number" placeholder="edit doctor contact number" name="number"  value="{{ $doctor->number }}">
			    @error('number')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			  <div class="form-group">
			    <label for="identity_number">No Identitas(KTP/Passport)</label>
			    <input type="text" class="form-control @error('identity_number') is-invalid @enderror" id="identity_number" placeholder="edit doctor contact identity_number" name="identity_number"  value="{{ $doctor->identity_number}}">
			    @error('identity_number')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
			  </div>
			  <div class="form-group">
				    <label for="username">username</label>
				    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="input username" name="username"  value="{{ $doctor->username}}">
				    @error('username')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
				  </div>				  
			  <button type="submit" class="btn btn-primary">edit</button>
			  <a href="{{ url('/doctor') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div>
</div>

  <!--/.row-->
@endsection
