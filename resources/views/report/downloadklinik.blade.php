 @extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">Laporan</h3>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			
				
				<a href="{{ url('/excelklinik') }}" class="btn btn-secondary">cetak excel</a>
				<a href="{{ url('/pdfklinik') }}" class="btn btn-secondary">cetak pdf</a>			

	  	  </div>
	  	</div>

	  </div>

	</div>
  </div><!--/.row-->
@endsection
