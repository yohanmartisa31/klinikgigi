 @extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
		<div class="panel panel-headline col-md-6">
	    <div class="panel-heading ">
		  <div class="row">
			
				<h3 class="panel-title">Laporan  Karyawan dan dokter</h3>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form method="post" action="/report">
			  @csrf			  
			  <div class="form-group" style="width:80%;">
			    <label for="tgl_mulai"> Tanggal mulai</label>
			    <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" placeholder="input no antrian" value="{{ old('tgl_mulai')}}">

			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>
			<div class="form-group" style="width:80%;">
			    <label for="tgl_selesai"> Tanggal selesai</label>
			    <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" placeholder="input no antrian" value="{{ old('tgl_selesai')}}">
			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>
			<div class="form-group" style="width:80%;">
			    <label for="name"> masukan nama karyawan atau dokter </label>
			    <input type="text" class="form-control" id="name" name="name" placeholder="input no antrian" value="{{ old('tgl_selesai')}}">
			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>
				<button type="submit" class="btn btn-primary">lihat</button>				
				<a href="{{ url('/payment') }}" class="btn btn-secondary">Back</a>
			</form>			
	  	  </div>
	  	</div>

	  </div>
<div class="panel panel-headline col-md-5"style="margin-left: 20px;">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">Laporan</h3>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form method="post" action="/reportklinik">
			  @csrf			  
			  <div class="form-group" style="width:80%;">
			    <label for="tgl_mulai"> Tanggal mulai</label>
			    <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" placeholder="input no antrian" value="{{ old('tgl_mulai')}}">

			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>
			<div class="form-group" style="width:80%;">
			    <label for="tgl_selesai"> Tanggal selesai</label>
			    <input type="date" class="form-control" id="tgl_selesai" name="tgl_selesai" placeholder="input no antrian" value="{{ old('tgl_selesai')}}">
			    @error('id')
	              <div class="invalid-feedback">
				{{ $message }}
				</div>
				@enderror			
			</div>			
				<button type="submit" class="btn btn-primary">lihat</button>				
				<a href="{{ url('/payment') }}" class="btn btn-secondary">Back</a>
			</form>			
	  	  </div>
	  	</div>

	  </div>
	</div>
  </div><!--/.row-->
@endsection
