@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">			
				<h3 class="panel-title">DATA MASTER (JADWAL DOKTER)</h3>			    			    
		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="main-content">
		<div class="container-fluid">
		<div class="panel-body"style="">
		  <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			@if($doctor->count() == 0)
      				<h3>tidak ada dokter yang berada di kategory </h3>
      		@else
				<form method="post" action="/schedule_doctor_save"style="margin-top: 50px;">
				@csrf			  							
				<!-- <select type="hidden" class="form-control" id="category_doctor"  name="category_doctor" style="display: none">
				  <option value="{{$category}}" selected  hidden> 
				  </option>
				</select>		   			 -->
				<div class="form-group">
				    <label for="name">nama dokter</label>
				    <select type="text" class="form-control @error('name') is-invalid @enderror" id="name"  name="name">
					  @error('name')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
					  <option value="none" selected disabled hidden> 
	          			Select name doctor 
	      			  </option> 
	      			  @if($doctor->count() == 0)
	      				<option value="" disabled>tidak ada list dokter</option>  			 		  
	      			  @else
	      			  @foreach($doctor as $temp)
	  			 		<option value="{{$temp->id}}">{{$temp->name}}</option>  			 		
	  				  @endforeach					  
	  				  @endif
					</select>
				  </div>
				  <div class="form-group">
				    <label for="day">hari praktek</label>
				    <select type="text" class="form-control @error('day') is-invalid @enderror" id="day"  name="day">
					  @error('day')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
					  <option value="none" selected disabled hidden> 
	          			Select day 
	      			  </option> 
	      			  @if($doctor->count() == 0)
	      				<option value="" disabled>tidak hari yang bisa dipilih</option>  			 		  
	      			  @else
	      			  @foreach($day as $temp)
	  			 		<option value="{{$temp->id}}">{{$temp->nama_hari}}</option>  			 		
	  				  @endforeach					  
	  				  @endif
					</select>
				  </div>
				  <div class="form-group col-md 5">
	                <label>Mulai *</label>        
	                
	                    <input name="mulai" data-mask class="form-control" type="time" required>                            
	                
	              </div>
	              <div class="form-group col-md 5">
	                <label>Selesai *</label>        
	                
	                    <input name="selesai" class="form-control" type="time" min="00:00:00"data-mask required>            
	                
	               </div>
	               <div class="form-group">
				    <label for="room">ruangan praktek</label>

				    <select type="text" class="form-control @error('room') is-invalid @enderror" id="room"  name="room">
					  @error('room')
					    <div class="invalid-feedback">
					      {{ $message }}
					    </div>
					  @enderror
					  <option value="none" selected disabled hidden> 
	          			Select room 
	      			  </option> 
	      			  @if($room_used->count() == 0)
	      				<option value="" disabled>tidak ada list ruangan</option>  			 		  
	      			  @else
	      			  @foreach($room_used as $temp)
	  			 		<option value="{{$temp->nama}}">{{$temp->nama}}</option>  			 		
	  				  @endforeach					  
	  				  @endif
					</select>
				  </div>

				  <button type="submit" class="btn btn-success">cari ruangan</button>						  
				</form>
			@endif
		  </div>			
		</div>
	  </div>
	</div>
</div>
  </div>
</div>
</div>

  <!--/.row-->
@endsection
