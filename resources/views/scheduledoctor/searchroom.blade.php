@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA MASTER (JADWAL DOKTER)</h3>			    			    

		  </div>
		  <!-- @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif -->
		</div>
		<div class="main-content">
		<div class="container-fluid">
		<div class="panel-body"style="">
		  <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
		  	
			<form method="post" action="/schedule_doctor_room"style="margin-top: 50px;">
			@csrf			  							
			  <div class="form-group">
			    <label for="room">ruangan praktek</label>

			    <select type="text" class="form-control @error('room') is-invalid @enderror" id="room"  name="room">
				  @error('room')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
				  <option value="none" selected disabled hidden> 
          			Select room 
      			  </option> 
      			  @if($jadwal->count() == 0)
      			  	@foreach($ruangan as $temp)
      				<option value="" >{{$temp->ruangan}}</option>  			 		  
      				@endforeach
      			 @else      			 	
      			 	@foreach($jadwal as $temp2)	
      			 		@if($temp->ruangan != $ruangan->id)
      			 		   <option value="" >{{$temp->ruangan}}</option>  			 		  
      			 		@endif
      			 	@endforeach
  				  @endif
				</select>
			  </div>
			  <button type="submit" class="btn btn-success">cari ruangan</button>						  
			</form>

		  </div>			
		</div>
	  </div>
	</div>
</div>
  </div>
</div>
</div>

  <!--/.row-->
@endsection
