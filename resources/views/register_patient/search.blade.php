@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
	  <div class="container-fluid">	
	    <div class="row">			  
          <div class="col-md-8">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">PASIEN</h3>			    			    

				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
						<form method="post" action="/layanan_schedule/search"style="margin-top: 50px;">
							
							@csrf			  							
							<div class="form-group">
			    				<label for="name">Nama Patient</label>
			    				<input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="input patient name" name="name" value="{{ old('name')}}">
								  @error('name')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
			    			</div>
			    			<div class="form-group">
			    				<label for="number">No.Telepon</label>
			    				<input type="text" class="form-control @error('number') is-invalid @enderror" id="number" placeholder="input patient number" name="number" value="{{ old('number')}}">
								  @error('number')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
			    			</div>
			    		<button type="submit" class="btn btn-success">cari pasien</button>						  
			    	</form>
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
          <div class="col-md-4">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">JADWAL DOKTER</h3>			    			    
					<h3>HARI:{{$day->nama_hari}}</h3>
				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			  		  @foreach($data as $temp)		  		
			  		  @if($temp == null)
			  		  	<h4>tidak ada data</h4>
			  		  @else
			  		    <h4>{{$loop->iteration}}. {{$temp->name}} | {{$temp->category}}| {{$temp->mulai}}-{{$temp->selesai}}</h4>
			  	      @endif
			  	      @endforeach
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
        </div>

        <div class="row">			  
          <div class="col-md-12">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">List pasien yang telah mendaftar</h3>			    			    

				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
				   <table class="table table-bordered">
					<thead class="thead">
					  <tr class="table-primary">
					  	<th scope="col">No</th>				  					  		  
					  	<th scope="col">No pendaftaran</th>
					  	<th scope="col">keluhan</th>
					  	<th scope="col">Id doctor</th>
					  	<th scope="col">Id pasien</th>
					  	
					  </tr>
					</thead>
					<tbody>
					  @foreach( $listregis as $patient ) 
					  <tr>
					  	<th scope="row">{{ $loop->iteration }}</th>				  					  	
					  	<td  class="col-sm-2">{{ $patient->no_pendaftaran }}</td>
					  	<td  class="col-sm-2">{{ $patient->complaint }}</td>
					  	<td  class="col-sm-3">{{ $patient->id_doctor }}</td>
					  	<td  class="col-sm-2">{{ $patient->patient_id }}</td>
					  	
					  </tr>
					  @endforeach
					</tbody>
				  </table>
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	          	
        </div>

      </div>
    </div>
</div>

  <!--/.row-->
@endsection
