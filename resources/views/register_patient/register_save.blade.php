@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
	  <div class="container-fluid">	
	    <div class="row">			  
          <div class="col-md-8">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">PASIEN</h3>			    			    

				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			       	<h4>No pendaftaran : 1</h4>
			       	<h4>Nama pasien : {{$patient->name}}</h4>
			       	<h4>Tanggal :{{$date}}</h4>			       	
			       	<h4>Umur : 19 </h4>
					<form method="post" action="/layanan_schedule_save"style="margin-bottom: 50px;">
							@csrf			  							
							<div class="form-group">
			    				<label for="complain">Keluhan</label>

			    				<textarea type="text" class="form-control @error('complain') is-invalid @enderror" id="complain" placeholder="input complain" name="complain" value="{{ old('complain')}}">
			    				</textarea>	
								  @error('complain')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
			    			</div>
			    			<div class="form-group">
			    				<label for="doctor">Nama Dokter</label>
			    				<select type="text" class="form-control @error('doctor') is-invalid @enderror" id="doctor"  name="doctor">
								  @error('jadwal')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
								  <option value="none" selected disabled hidden> 
				          			Select doctor available 
				      			  </option> 				      			  
				    			  @foreach($data as $temp)		  		
							  		  @if($temp == null)
							  		  	<option value="" disabled hidden>tidak ada list dokter</option>  			 	  
							  		  @else
				  			 			<option value="{{$temp->doctor}}">{{$temp->name}} | {{$temp->category}} | {{$temp->mulai}}-{{$temp->selesai}}</option>
							  	      @endif
							  	  @endforeach
								</select>  
			    			</div>
			    		<button type="submit" class="btn btn-success">Submit</button>						  
			    	</form>
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
          <div class="col-md-4">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">JADWAL DOKTER</h3>			    			    
					<h3>HARI:{{$day->nama_hari}}</h3>
				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			  		  @foreach($data as $temp)		  		
			  		  @if($temp == null)
			  		  	<h4>tidak ada data</h4>
			  		  @else
			  		    <h4>{{$loop->iteration}}. {{$temp->name}} | {{$temp->category}}| {{$temp->mulai}}-{{$temp->selesai}}</h4>
			  	      @endif
			  	      @endforeach
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
        </div>

      </div>
    </div>
</div>

  <!--/.row-->
@endsection
