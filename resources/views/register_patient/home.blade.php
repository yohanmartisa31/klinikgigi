@extends('template')

@section('content')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA MASTER (JADWAL DOKTER)</h3>			    			    

		  </div>
		  @if (session('status'))
		    <div class="alert alert-warning">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="main-content">
		<div class="container-fluid">
		<div class="panel-body"style="margin-top:5%;">
		  <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">
  			<form method="post" action="/layanan_schedule">
			  @csrf			  
			  <div class="form-group">
			    <label for="category_doctor">Kategory Doktor</label>
			    <select type="text" class="form-control @error('category_doctor') is-invalid @enderror" id="category_doctor"  name="category_doctor">
				  @error('category_doctor')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror
				  <option value="none" selected disabled hidden> 
          			Select category doctor 
      			  </option> 
      			  	  <option value="umum">umum (Dr. g)</option>
					  <option value="Spesialis Kedokteran Gigi Anak (Sp. KGA)">Spesialis Kedokteran Gigi Anak (Sp. KGA)</option>
					  <option value="Spesialis Konservasi Gigi (Sp. KG)">Spesialis Konservasi Gigi (Sp. KG)</option>
					  <option value="Spesialis Penyakit Mulut (Sp. PM)">Spesialis Penyakit Mulut (Sp. PM)</option>
					  <option value="Spesialis Ortodonsia (Sp. Ort)">Spesialis Ortodonsia (Sp. Ort)</option>
					  <option value="Spesialis Periodonsia (Sp. Perio)">Spesialis Periodonsia (Sp. Perio)</option>
					  <option value="Spesialis Prostodonsia (Sp. Pros)">Spesialis Prostodonsia (Sp. Pros)</option>
					  <option value="Spesialis Radiologi Kedokteran Gigi (Sp. RKG)">Spesialis Radiologi Kedokteran Gigi (Sp. RKG)</option>
					  <option value="Spesialis Bedah Mulut (Sp. BM)">Spesialis Bedah Mulut (Sp. BM)</option>			
				</select>
			  </div>
			  <button type="submit" class="btn btn-success">cari</button>						  
			</form>

		  </div>			
		</div>
	  </div>
	</div>
</div>
  </div>
</div>
</div>

  <!--/.row-->
@endsection
