@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
	  <div class="container-fluid">	
	    <div class="row">			  
          <div class="col-md-8">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">PASIEN</h3>			    			    

				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
					<form method="post" action="/layanan_schedule/search"style="margin-bottom: 50px;">
							@csrf			  							
							<div class="form-group">
			    				<label for="name">Nama Patient</label>
			    				<input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="input patient name" name="name" value="{{ old('name')}}">
								  @error('name')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
			    			</div>
			    			<div class="form-group">
			    				<label for="number">No.Telepon</label>
			    				<input type="text" class="form-control @error('number') is-invalid @enderror" id="number" placeholder="input patient number" name="number" value="{{ old('number')}}">
								  @error('number')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
			    			</div>
			    		<button type="submit" class="btn btn-success">cari pasien</button>						  
			    	</form>
					<table class="table table-bordered">
						<thead class="thead">
						  <tr class="table-primary">
						  	<th scope="col">No</th>				  	
						  	<th scope="col">Nama pasien</th>
						  	<th scope="col">jenis kelamin</th>
						  	<th scope="col">Alamat</th>				  					  	
						  	<th scope="col">No identitas</th>
						  	<th scope="col">No handphone</th>				  	
						  	<th scope="col">Action</th>
						  </tr>
						</thead>
						<tbody>
						  @foreach( $patient as $temp ) 
						  <tr>
						  	<th scope="row">{{ $loop->iteration }}</th>
						  	<td class="col-sm-2">{{ $temp->name }}</td>				  	
						  	<td  class="col-sm-3">{{ $temp->gender }}</td>
						  	<td  class="col-sm-2">{{ $temp->address }}</td>
						  	<td  class="col-sm-2">{{ $temp->number }}</td>
						  	<td  class="col-sm-2">{{ $temp->identity_number }}</td>
						  	
						  	<td class="col-sm-2">
				            <a href="/layanan_schedule/search/{{ $temp->id }}" class="btn btn-primary" style="width:100%;margin-bottom: 5px;">register </a>						  	
						  	</td>
						  </tr>
						  @endforeach
						</tbody>
					  </table>					
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
          <div class="col-md-4">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">JADWAL DOKTER</h3>			    			    
					<h3>HARI:{{$day->nama_hari}}</h3>
				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			  		  @foreach($data as $temp)		  		
			  		  @if($temp == null)
			  		  	<h4>tidak ada data</h4>
			  		  @else
					<h4>{{$loop->iteration}}. {{$temp->name}} | {{$temp->category}}| {{$temp->mulai}}-{{$temp->selesai}}</h4>
			  	      @endif
			  	      @endforeach
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
        </div>

      </div>
    </div>
</div>

  <!--/.row-->
@endsection
