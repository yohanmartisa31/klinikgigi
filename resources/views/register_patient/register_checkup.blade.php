@extends('template')

@section('content2')
  <div class="main" style="min-height: 0;">
	<div class="main-content">
	  <div class="container-fluid">	
	    <div class="row">			  
          <div class="col-md-8">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">PASIEN</h3>			    			    

				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			       	<h4>{{$patient->name}}</h4>
			       	<h4>{{$date}}</h4>			       	
					<form method="post" action="/layanan_schedule_category"style="margin-bottom: 50px;">
							@csrf			  														
			    			<div class="form-group">
			    				<label for="category_doctor">Kategory Doktor</label>
							    <select type="text" class="form-control @error('category_doctor') is-invalid @enderror" id="category_doctor"  name="category_doctor">
								  @error('category_doctor')
								    <div class="invalid-feedback">
								      {{ $message }}
								    </div>
								  @enderror
								  <option value="none" selected disabled hidden> 
				          			Select category doctor 
				      			  </option> 
				      			  	  <option value="umum">umum (Dr. g)</option>
									  <option value="Spesialis Kedokteran Gigi Anak (Sp. KGA)">Spesialis Kedokteran Gigi Anak (Sp. KGA)</option>
									  <option value="Spesialis Konservasi Gigi (Sp. KG)">Spesialis Konservasi Gigi (Sp. KG)</option>
									  <option value="Spesialis Penyakit Mulut (Sp. PM)">Spesialis Penyakit Mulut (Sp. PM)</option>
									  <option value="Spesialis Ortodonsia (Sp. Ort)">Spesialis Ortodonsia (Sp. Ort)</option>
									  <option value="Spesialis Periodonsia (Sp. Perio)">Spesialis Periodonsia (Sp. Perio)</option>
									  <option value="Spesialis Prostodonsia (Sp. Pros)">Spesialis Prostodonsia (Sp. Pros)</option>
									  <option value="Spesialis Radiologi Kedokteran Gigi (Sp. RKG)">Spesialis Radiologi Kedokteran Gigi (Sp. RKG)</option>
									  <option value="Spesialis Bedah Mulut (Sp. BM)">Spesialis Bedah Mulut (Sp. BM)</option>			
								</select>
			    			</div>
			    		<button type="submit" class="btn btn-success">cari dokter</button>						  
			    	</form>
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
          <div class="col-md-4">				<!-- OVERVIEW -->
		   <div class="panel panel-headline">
	         <div class="panel-heading">

				<h3 class="panel-title">JADWAL DOKTER</h3>			    			    
					<h3>HARI:{{$day->nama_hari}}</h3>
				  @if (session('status'))
		    		 <div class="alert alert-success">
		      			{{ session('status') }}
		     		 </div>
		  		  @endif
		     </div>
		     <div class="main-content">
		       <div class="container-fluid">
		         <div class="panel-body"style="">		
			       <div class="canvas-wrapper" height="1000"style="overflow-x:auto;">  			
			  		  @foreach($data as $temp)		  		
			  		  @if($temp == null)
			  		  	<h4>tidak ada data</h4>
			  		  @else
			  		    <h4>{{$loop->iteration}}. {{$temp->name}} | {{$temp->category}}| {{$temp->mulai}}-{{$temp->selesai}}</h4>
			  	      @endif
			  	      @endforeach
			       </div>		
		         </div>
	  		   </div>
			 </div>
           </div>
          </div>	
        </div>

      </div>
    </div>
</div>

  <!--/.row-->
@endsection
