@extends('template')

@section('content')
  <div class="main">
	<div class="main-content">
		<div class="container-fluid">
					<!-- OVERVIEW -->
			<div class="panel panel-headline">
	    <div class="panel-heading">
		  <div class="row">
			
				<h3 class="panel-title">DATA PASIEN</h3>			    
			    <a href="{{ url('/patient/register') }}" class="btn btn-success"><i class="lnr lnr-user"></i><span> Register Pasien baru</span></a>			

		  </div>
		  @if (session('status'))
		    <div class="alert alert-success">
		      {{ session('status') }}
		    </div>
		  @endif
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="1000" style="overflow-x:auto;">
			  <table class="table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">
				  	<th scope="col">No</th>				  					  		  
				  	<th scope="col">Nama pasien</th>
				  	<th scope="col">Jenis Kelamin</th>
				  	<th scope="col">Alamat</th>
				  	<th scope="col">No.telepon</th>
				  	<th scope="col">Action</th>
				  </tr>
				</thead>
				<tbody>
				  @foreach( $patient as $patient ) 
				  <tr>
				  	<th scope="row">{{ $loop->iteration }}</th>				  					  	
				  	<td  class="col-sm-2">{{ $patient->name }}</td>
				  	<td  class="col-sm-2">{{ $patient->gender }}</td>
				  	<td  class="col-sm-3">{{ $patient->address }}</td>
				  	<td  class="col-sm-2">{{ $patient->number }}</td>
				  	<td class="col-sm-3">
		            <a href="/patient/{{ $patient->id }}/edit" class="btn btn-primary">Edit</a>			
		            <a href="/patient/{{ $patient->id }}/edit" class="btn btn-warning">Detail</a>		
		            <!-- <a href="/patient/{{ $patient->id }}/registerservice" class="btn btn-warning">Daftar pelayanan</a> -->
				  	</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
		  </div>
			<!-- <table class="table">
			  <thead class="thead-primary">
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">First</th>
			      <th scope="col">Last</th>
			      <th scope="col">Handle</th>
			    </tr>
			  </thead>
			  <tbody>
			    <tr>
			      <th scope="row">1</th>
			      <td>Mark</td>
			      <td>Otto</td>
			      <td>@mdo</td>
			    </tr>
			    <tr>
			      <th scope="row">2</th>
			      <td>Jacob</td>
			      <td>Thornton</td>
			      <td>@fat</td>
			    </tr>
			    <tr>
			      <th scope="row">3</th>
			      <td>Larry</td>
			      <td>the Bird</td>
			      <td>@twitter</td>
			    </tr>
			  </tbody>
			</table> -->
		  <!-- </div> -->
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
