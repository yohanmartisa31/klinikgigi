<style>
#antrian_pdf {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 80%;
}

#antrian_pdf td, #antrian_pdf th {
  border: 1px solid #ddd;
  padding: 8px;
  text-align: center;
}

#antrian_pdf tr:nth-child(even){background-color: #f2f2f2;}

#antrian_pdf tr:hover {background-color: #ddd;}
#antrian_pdf th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
#antrian_pdf th #id{
    width:20%;
}
#antrian_pdf th #nama{
    width:80%;
}
</style>
<div class="row col-12">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <div class="row">
              <div class="col-6"><h2>LAPORAN PENDAPATAN</h2></div>                          
              <p>Tanggal mulai :{{Session('mulai')}}</p>
              <p>Tanggal selesai :{{Session('selesai')}}</p>
          </div>
        </div>
        <div class="panel-body">
          <div class="canvas-wrapper col-md-8" height="600">            
            <table id="antrian_pdf">
                  <tr>
                    <th style="text-align: center;">no</th>                                      
                    <th style="text-align: center;">tanggal</th>                  
                    <th style="text-align: center;">nama pasien</th>                                     
                    <th style="text-align: center;">pendapatan</th>                                      
                  </tr>
                  <?php $total = 0;$no= 0; ?>                              
                  @foreach(Session('transaksi') as $temp)                                                 
                    <tr>
                    @if($temp->tanggal >= Session('mulai')) && ($temp->tanggal <= Session('selesai')))
                      <td><h3>{{ $no++ }}</h3></td>                                      
                      <td><h3>{{ $temp->tanggal }}</h3></td>                                      
                      <td><h3>{{ $temp->nama_pasien }}</h3></td>                                                      
                      <?php $harga = $temp->harga *20/100; $total=$total+$harga ?>                        
                      <td><h3>Rp.{{ $harga }}</h3></td>                       
                      <?php $harga = $temp->harga / 100; $total=$total+$harga ?>                      
                      <td><h3>Rp.{{ $harga }}</h3></td>                     
                    @endif                             
                    </tr>
                  @endforeach
                  <h3>Total Pendapatan : Rp.{{$total}}</h3>
            </table>
          </div>            
        </div>
      </div>
    </div>
  </div><!--/.row-->