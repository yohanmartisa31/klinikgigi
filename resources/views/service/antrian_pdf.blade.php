<style>
#antrian_pdf {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 80%;
}

#antrian_pdf td, #antrian_pdf th {
  border: 1px solid #ddd;
  padding: 8px;
  text-align: center;
}

#antrian_pdf tr:nth-child(even){background-color: #f2f2f2;}

#antrian_pdf tr:hover {background-color: #ddd;}
#antrian_pdf th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
#antrian_pdf th #id{
	width:20%;
}
#antrian_pdf th #nama{
	width:80%;
}
</style>
<div class="row col-12">
	<div class="col-md-12">
	  <div class="panel panel-default">
	    <div class="panel-heading">
		  <div class="row">
			  <div class="col-6"><h2>LAPORAN PENDAPATAN</h2></div>			  
			  <h2>Nama :{{Session('name')}}</h2>
			  <p>Kategory :{{Session('category')}}</p>
			  <p>Tanggal mulai :{{Session('mulai')}}</p>
			  <p>Tanggal selesai :{{Session('selesai')}}</p>
		  </div>
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper col-md-8" height="600">		  	
			<table id="antrian_pdf">
				  <tr>
				  	<th style="width:80%;text-align: center;">tanggal</th>				  	
				  	<th style="width:100%;text-align: center;">pendapatan</th>				  					  	
				  </tr>
				  <?php $total = 0; ?>			   					
				  @foreach(Session('transaksi') as $temp)		  					  					  
				  <tr>
				  	<td><h3>{{ $temp->tanggal }}</h3></td>				  	
				  	@if($temp->nama_dokter != null))
				  	<?php $harga = $temp->harga *20/100; $total=$total+$harga ?>			   			
				  	<td><h3>Rp.{{ $harga }}</h3></td>	
				  	@else
				  	<?php $harga = $temp->harga / 100; $total=$total+$harga ?>			   			
				  	<td><h3>Rp.{{ $harga }}</h3></td>	
				  	@endif			  					  
				  </tr>				  				
				  @endforeach
				  <h3>Total Pendapatan : Rp.{{$total}}</h3>
			</table>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->