@extends('layout/main')

@section('title', 'Print_form')

@section('container')
  <div class="row col-12">
	<div class="col-md-12">
	  <div class="panel panel-default">
	    <div class="panel-heading">
		  <div class="row">
			  <div class="col-6">Print Form</div>			  
		  </div>
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper col-md-6" height="600">
		  	<a href="/patient/cetak_pdf" class="btn btn-primary" target="_blank" style="margin-bottom:5%;">PDF</a>
			<table class="display table table-bordered">
				<thead class="thead">
				  <tr class="table-primary">
				  	<th scope="col-sm-1">No.Antrian</th>
				  	<th scope="col-md-8">Nama</th>
				  	<th scope="col-md-8">category doctor</th>				  	
				  	<th scope="col-md-8">doctor</th>				  					  	
				  </tr>
				</thead>
				<tbody>				 
				  <tr>
				  	<th class="col-sm-1"><h3>{{ $no->id }}</h3></th>
				  	<td class="col-md-8"><h3>{{ $no->name }}</h3></td>				  	
				  	<td class="col-md-8"><h3>{{ $no->category }}</h3></td>
				  	<td class="col-md-8"><h3>{{ $no->doctor }}</h3></td>
				  </tr>				  
				</tbody>
			</table>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
