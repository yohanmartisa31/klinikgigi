@extends('layout/main')

@section('title', 'Register Services')

@section('container')
  <div class="row col-12">
	<div class="col-md-12">
	  <div class="panel panel-default">
	    <div class="panel-heading">
		  <div class="row">
			  <div class="col-6">Register Services</div>			  
		  </div>
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/patient/{{ $patient->id}}/createsession" method="post" class="d-inline">			  
    				@csrf
			  <!-- <div class="form-group">
			    <label for="id_patient">id_patient</label>
			    <input type="text" class="form-control" id="id_patient" placeholder="input id_patient" name="id_patient">
			  </div> -->			  
			  <h3>Patient ID: {{ $patient->id }}</h3>
			  <h3>Patient Name: {{ $patient->name }}</h3>
			  <div class="form-group">
			    <label for="category">Category Doctor</label>
			    <select type="text" class="form-control" id="category"  name="category">	
				  <option value="none" selected disabled hidden> 
          			Select category doctor 
      			  </option> 
				  <option>umum</option>
				  <option>spesialis</option>				      	
			    </select>
			  </div> 			  
			  <div class="form-group">		 	    
			    <input type="hidden" class="form-control" id="patient_id" name="patient_id" value="{{ $patient->id }}">				
			  </div>
			  <div class="form-group">			    
			    <input type="hidden" class="form-control" id="name" name="name" value="{{ $patient->name }}">		
			  </div>
			  
			  <div class="form-group">
			    <label for="complaint">Complaint</label>
			    <textarea type="text" rows="5" class="form-control @error('complaint') is-invalid @enderror" id="complaint"  name="complaint"  value="{{ old('complaint') }}">
			    @error('complaint')
				    <div class="invalid-feedback">
				      {{ $message }}
				    </div>
				  @enderror			
				</textarea>
			  </div>			  
			  <button type="submit" class="btn btn-primary">register service</button>
			  <a href="{{ url('/patient') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
