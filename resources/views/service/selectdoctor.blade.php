@extends('layout/main')

@section('title', 'Register Services')

@section('container')
  <div class="row col-12">
	<div class="col-md-12">
	  <div class="panel panel-default">
	    <div class="panel-heading">
		  <div class="row">
			  <div class="col-6">Register Services</div>			  
		  </div>
		</div>
		<div class="panel-body">
		  <div class="canvas-wrapper" height="600">
			<form action="/patient/registerservice" method="post" class="d-inline">			  
    				@csrf
			  <!-- <div class="form-group">
			    <label for="id_patient">id_patient</label>
			    <input type="text" class="form-control" id="id_patient" placeholder="input id_patient" name="id_patient">
			  </div> -->			  
			  <h1>{{$complaint}}</h1>
			  <div class="form-group">		 	    
			    <input type="hidden" class="form-control" id="patient_id" name="patient_id" value="{{ $patient_id }}">	
			  <div class="form-group">			    
			    <input type="hidden" class="form-control" id="name" name="name" value="{{ $name }}">		
			  </div>
			  <div class="form-group">		 	    
			    <input type="hidden" class="form-control" id="category" name="category" value="{{ $category }}">	
			  </div>
			  <div class="form-group">			    
			    <input type="hidden" class="form-control" id="name" name="complaint" value="{{ $complaint }}">		
			  </div>
			  <div class="form-group">
			    <label for="doctor">Name doctor</label>
			    <select type="text" class="form-control @error('name') is-invalid @enderror" id="doctor" name="doctor">	
			  	<option value="none" selected disabled hidden> 
          			Select name doctor
      			  </option> 
      			  <h2>$category</h2>
			  @foreach( $doctor as $doctor )			  	
			  	@if( $doctor->category == $category)
			    <option>{{$doctor->name}}</option>
			    @endif	  
			  @endforeach
			  	</select>
			  </div> 
			  <button type="submit" class="btn btn-primary">register service</button>
			  <a href="{{ url('/patient') }}" class="btn btn-secondary">Back</a>
			</form>
		  </div>			
		</div>
	  </div>
	</div>
  </div><!--/.row-->
@endsection
