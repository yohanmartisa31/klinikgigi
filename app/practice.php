<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class practice extends Model
{
    //
    protected $fillable = ['name_practice', 'category_doctor', 'price'];        
    public function getPrice()
    {
        return $this->belongsTo('App\price', 'price');
    }
}
