<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UserReportklinik implements FromView
{

    
    public function view(): View
    {
    	
        return view('excel.userklinik', [
        'transaksi' => \App\transaksi::get(),                
        ]);
    }
}
