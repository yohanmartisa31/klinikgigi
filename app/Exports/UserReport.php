<?php
namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class UserReport implements FromView
{

    
    public function view(): View
    {
    	
        return view('excel.user', [
        'transaksi' => \App\transaksi::get(),                
        ]);
    }
}
