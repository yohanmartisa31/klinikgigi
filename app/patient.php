<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class patient extends Model
{
    //
    protected $fillable = ['name', 'number','place_of_birth','date_of_birth','gender','address','religion','job','citizenship','identity_number'];
    
            
}
