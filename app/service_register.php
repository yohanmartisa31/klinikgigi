<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service_register extends Model
{
    //
        protected $primaryKey = 'id';


        protected $table = 'service_register';
        protected $fillable = ['no_pendaftaran', 'complaint','id_doctor', 'patient_id', 'tanggal','cek'];
    public function getPatient()
    {
        return $this->belongsTo('App\patient', 'patient_id');
    }
    public function getDoctor()
    {
        return $this->belongsTo('App\doctor', 'id_doctor');
    }
}
