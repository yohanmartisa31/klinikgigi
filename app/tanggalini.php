<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tanggalini extends Model 
{

    protected $table = 'tanggalini';
    public $timestamps = true;
    protected $fillable = array('tanggal');

}