<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catatan_pemeriksaan extends Model
{
    //ini yang bawah satu baris hapus aja soalnya di urang ribet mesti migrate ulang
    protected $table = 'catatan_pemeriksaan';
    protected $cast = [
 	  'practice' => 'array'
    ];        
    protected $fillable = ['complaint', 'diagnosa','catatan','id_employee', 'id_service_register', 'id_doctor', 'id_jenis_pelayanan', 'tanggal', 'price', 'nama_dokter', 'nama_pasien', 'nama_perawat', 'nama_jenis_pelayanan', 'no_pendaftaran'];        
}
