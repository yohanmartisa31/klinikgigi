<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class price extends Model
{
    //
    protected $table = 'price';
    protected $fillable = ['name_practice', 'category_doctor', 'price', 'tanggal_update_harga'];             
}
