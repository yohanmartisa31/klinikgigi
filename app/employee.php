<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employee extends Model
{
    //
    protected $fillable = ['name', 'category','gender','address', 'number', 'identity_number', 'username', 'password', 'pass_reset_date', 'reset_by'];        
}
