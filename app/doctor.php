<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    //
    protected $fillable = ['name', 'category', 'place_of_birth','date_of_birth','gender','address','religion', 'number', 'identity_number', 'username', 'password', 'pass_reset_date', 'reset_by'];        
}
