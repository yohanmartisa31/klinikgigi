<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jadwal extends Model 
{

    protected $table = 'ruangan';
    public $timestamps = true;
    protected $fillable = array('hari', 'doctor', 'ruangan', 'mulai', 'selesai', 'ready');

    public function getHari()
    {
        return $this->belongsTo('App\hari', 'hari');
    }
    public function getDoctor()
    {
    	    return $this->belongsTo('App\doctor', 'doctor');
	}
}