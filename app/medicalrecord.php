<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicalrecord extends Model
{
    //ini yang bawah satu baris hapus aja soalnya di urang ribet mesti migrate ulang
    
    protected $cast = [
 	  'practice' => 'array'
    ];        
    protected $fillable = ['name', 'complaint', 'diagnosa','note','patient_id', 'practice'];        
}
