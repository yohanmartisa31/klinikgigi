<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
      $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        $listpatient = \App\service_register::where('cek','1')->get();
        return view('payment/home',compact('listpatient', 'day'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function save(Request$request)
    {
      $fo = \App\ModelUser::where('username',Session('username'))->get()->first();
      $cekpasien = \App\transaksi::where('nama_pasien',Session('catatan_pemeriksaan')->nama_pasien)->get()->count();      
       $dokter = Session('catatan_pemeriksaan')->nama_dokter;
       // dd(Session('catatan_pemeriksaan')->id_doctor);
    
      if($cekpasien !=0){
          if(Session('tambahan')==null){              
              \App\transaksi::create([
              'id_catatan_pemeriksaan' =>Session('catatan_pemeriksaan')->id,
              'id_karyawan' => Session('catatan_pemeriksaan')->id_employee,              
              'tanggal' => Session('catatan_pemeriksaan')->tanggal,        
              'nama_jenis_pelayanan' => Session('catatan_pemeriksaan')->nama_jenis_pelayanan,
              'harga' => $request->total,
              'nama_front_office' => $fo->username,
              'kategori_transaksi' => 'pelayanan',
              'cara_pembayaran' => $request->metode,
              'nama_perawat' => Session('catatan_pemeriksaan')->nama_perawat,              
              'nama_pasien' => Session('catatan_pemeriksaan')->nama_pasien,
              'produk_tambahan' =>Session('tambahan'),
              'status' => '1',
              'created_at'
              ]);              
          }
          else{           
              \App\transaksi::create([
              'id_catatan_pemeriksaan' =>Session('catatan_pemeriksaan')->id,
              'id_karyawan' => Session('catatan_pemeriksaan')->id_employee,              
              'tanggal' => Session('catatan_pemeriksaan')->tanggal,        
              'nama_jenis_pelayanan' => Session('catatan_pemeriksaan')->nama_jenis_pelayanan,
              'harga' => $request->total,
              'nama_front_office' => $fo->username,
              'kategori_transaksi' => 'produk',
              'cara_pembayaran' => $request->metode,
              'nama_perawat' => Session('catatan_pemeriksaan')->nama_perawat,              
              'nama_pasien' => Session('catatan_pemeriksaan')->nama_pasien,
              'produk_tambahan' =>Session('tambahan'),
              'status' => '1'
              ]);              
          }
      }
      else{        
              \App\transaksi::create([
              'id_catatan_pemeriksaan' =>Session('catatan_pemeriksaan')->id,
              'id_karyawan' => Session('catatan_pemeriksaan')->id_employee,              
              'tanggal' => Session('catatan_pemeriksaan')->tanggal,        
              'nama_jenis_pelayanan' => Session('catatan_pemeriksaan')->nama_jenis_pelayanan,
              'harga' => $request->total,
              'nama_front_office' => $fo->username,
              'kategori_transaksi' => 'admin',
              'cara_pembayaran' => $request->metode,
              'nama_perawat' => Session('catatan_pemeriksaan')->nama_perawat,              
              'nama_pasien' => Session('catatan_pemeriksaan')->nama_pasien,
              'produk_tambahan' =>Session('tambahan'),
              'status' => '1'
              ]);
      }
        // 'id_catatan_pemeriksaan' =>Session('catatan_pemeriksaan')->id,
        // 'id_karyawan' => Session('catatan_pemeriksaan')->id_employee,
        // 'tanggal' => Session('catatan_pemeriksaan')->tanggal,        
        // 'nama_jenis_pelayanan' => Session('catatan_pemeriksaan')->nama_jenis_pelayanan,
        // 'harga' => $request->total,
        // 'nama_front_office' => $fo->username,
        // 'kategori_transaksi' 
        // 'cara_pembayaran' => $request->metode,
        // 'nama_perawat' => Session('catatan_pemeriksaan')->nama_perawat,
        // 'nama_pasien' => Session('catatan_pemeriksaan')->nama_pasien,
        // 'nama_tambahan' =>Session('tambahan');
        // 'status'
    
    return redirect('/payment')->with('status', 'pembayaran selesai');
    }

    public function update(Request $request)
    {
      $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');      
      $catatan_pemeriksaan = Session('catatan_pemeriksaan');
       if($catatan_pemeriksaan == null){
        return redirect('/payment')->with('status', 'tidak ada pasien');
      }
      else{
        $data = json_decode($catatan_pemeriksaan->id_jenis_pelayanan);
        $i=0;      
        foreach ($data as $key) {
              # code...
          
          $jenispelayanan[$i] = \App\practice::where('id',$data[$i])->get('name_practice')->first();
          $harga[$i] = \App\practice::where('id',$data[$i])->get('price')->first();
          $ha[$i] =  DB::table('practices')
                  ->join('price','practices.price','=', 'price.id')                                
                  ->where('practices.price','=',$harga[$i]->price)
                  ->select('price.price')                                
                  ->get('price');        
          $i= $i+1;
          // // $data[$key] = $jenispelayanan[$key]->price;
          }
        $tambahan=$request->harga;
        $produk=$request->nama;
        Session::put('tambahan', $tambahan);
        Session::put('produk', $produk);
        $listpatient = \App\service_register::where('patient_id','$request->id')->get();
        return view('payment/bills', compact('catatan_pemeriksaan','date', 'jenispelayanan', 'harga', 'tambahan', 'produk'));
      }
    }  
     
    
    public function search(Request $request)
    {          
      // $practice= \App\practice::all();
      // $a=collect(json_decode($practice));
      // $b=json_decode($a);
      // $c=json_encode($b);
      // dd($practice);
      // $data =DB::table('service_register')
      //           ->join('medicalrecords', 'service_register.patient_id', '=', 'medicalrecords.patient_id')
      //           ->where('service_register.id', '=' , DB::raw($request->id))
      //           ->select( 'medicalrecords.*', 'service_register.*')
      //           ->orderBy('medicalrecords.created_at', 'desc')
      //           ->first();
        // dd($data);
      // $data->practice = json_decode($data->practice,true);
      // dd($data);
      $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');      
      $catatan_pemeriksaan = \App\catatan_pemeriksaan::where('no_pendaftaran', $request->no_pendaftaran)->where('tanggal',$date)->get()->first();      
      Session::put('catatan_pemeriksaan',$catatan_pemeriksaan);
      if($catatan_pemeriksaan == null){
        return redirect('/payment')->with('status', 'tidak ada pasien');
      }
      else{
      $transaksi = \App\transaksi::where('id_catatan_pemeriksaan', $catatan_pemeriksaan->id)->where('tanggal',$date)->get()->count();
      if($transaksi == 0){
        $data = json_decode($catatan_pemeriksaan->id_jenis_pelayanan);
        $i=0;      
        foreach ($data as $key) {
              # code...

          $jenispelayanan[$i] = \App\practice::where('id',$data[$i])->get('name_practice')->first();
          $harga[$i] = \App\practice::where('id',$data[$i])->get('price')->first();
          $ha[$i] =  DB::table('practices')
                  ->join('price','practices.price','=', 'price.id')                                
                  ->where('practices.price','=',$harga[$i]->price)
                  ->select('price.price')                                
                  ->get('price');        
          $i= $i+1;
          // // $data[$key] = $jenispelayanan[$key]->price;
          }
        $tambahan=0;
        $produk='';
        Session::put('tambahan', '');
        Session::put('produk', '');
        $listpatient = \App\service_register::where('patient_id','$request->id')->get();
        return view('payment/bills', compact('catatan_pemeriksaan','date', 'jenispelayanan', 'harga','tambahan','produk'));
      }
      else{
         return redirect('/payment')->with('status', 'pasien telah membayar');
      }
      }
    }
      

      // dd(json_decode($practice));
      // dd($practice);
        // dd($c);
      // $y=collect(json_decode($medicalrecords,true));
      // $z=json_decode($y);
      // $q=$z;
      // dd($q);
       // $coba1=json_extract($medicalrecords,true);
       // dd($coba1); 
      // $x=json_encode($q[1]);
      // if ($q[1] == $practice[1]->name_practice){
        // dd(json_encode($q[1]));
      //   }
      
      // $a=json_decode($medicalrecords,true);
      // $i=json_decode($a[1]);
      // dd(json_decode($a[1]));
        // $asli= DB::table('medicalrecords')
        //       ->where('practice',practice)
        //       // ->whereIn('medicalrecords', json_decode($y, true))
        //       ->get();
        // dd($asli);
        // $service =DB::table('medicalrecords')
        //         ->join('practices', 'medicalrecords->name_practice', '=', 'practices.json_col_name.practice')
        //         // ->join('practices', 'service_register.category_doctor', '=', 'practices.category_doctor')
      //           ->select('practices.price')
      //           ->get();
      //   dd($service);
      // $medicalrecords2= json_decode($service->practice);
      // $price = DB::table('medicalrecords')
      //           ->join('practices', 'medicalrecords.practice', '=', 'practices.name')           
      //           ->select('medicalrecords.*','practices.*',)
      //           ->get();

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    \App\service_register::destroy($id);
    return redirect('/payment')->with('status', 'Delete success');
    }
}
