<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class LayananController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $doctor = \App\doctor::all();
        return view('register_patient/home',['doctor' => $doctor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('doctor/register_doctor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
            $data = \App\service_register::get('no_pendaftaran')->last();
            if($data == null){
            // dd($data->no_pendaftaran);
            $data = 1;
            }
            else{
                $data = $data->no_pendaftaran+1;
            }
                
               // dd($request->doctor);     
               // $doctor = \App\doctor::where('name',$request->doctor)->get()->first();
            \App\service_register::create([
                            'no_pendaftaran' => $data,
                            'complaint' => $request->complain,
                            'id_doctor' => $request->doctor,
                            'patient_id'=>Session('id_patient'),
                            'tanggal'=>Session('date'), 
                            'cek' => '0',                            
            ]);
            \App\ruangan::where('doctor',$request->doctor)
                        ->update([
                            'ready'=>'1',
                        ]);
            return redirect('/layanan_schedule')->with('status', 'Register success');
    }
    
    public function regis($id)
    {
        //

        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        
        $patient = \App\patient::find($id);
        Session::put('id_patient',$patient->id);                  
        Session::put('date',$date);                  
        // $doctor = \App\doctor::where('category',Session('category_doctor'))->get('id');   
        // foreach($doctor as $n){
         //     $uu[] = [
        //         'id_doctor' => $n->id
        //     ];
        // }   
        // $i=0;           
        // foreach($doctor as $n){

        // $available[$i] = \App\jadwal::where('hari',$day->id)->where('doctor',$uu[$i])->first();                  
        $data =DB::table('ruangan')
                ->join('room_used', 'ruangan.ruangan', '=', 'room_used.id')
                ->join('doctors','ruangan.doctor', '=', 'doctors.id')
                ->where('room_used.hari', '=' , $day->id)
                ->select( 'ruangan.*', 'room_used.*', 'doctors.*')                
                ->orderBy('room_used.hari', 'desc')
                ->get();
        // $i=$i+1;
        // }
        // dd($available);
        // dd(count($available));
        
        return view('register_patient/register_checkup',compact('data','day','date', 'patient'));
    }
    public function category(Request $request)
    {
        // dd($request->category_doctor);
        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $patient = \App\patient::find(Session('id_patient'));
        
        $data =DB::table('ruangan')
                ->join('room_used', 'ruangan.ruangan', '=', 'room_used.id')
                ->join('doctors','ruangan.doctor', '=', 'doctors.id')
                ->where('room_used.hari', '=' , $day->id)
                ->where('doctors.category', '=', $request->category_doctor)
                ->select( 'ruangan.*', 'room_used.*', 'doctors.*')                
                ->orderBy('room_used.hari', 'desc')
                ->get();
        // dd(count($available));
        
        return view('register_patient/register_save',compact( 'data','day','date', 'patient'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        
        // $ruangan = \App\ruangan::get();                
        // $doctor = \App\doctor::where('category',$request->category_doctor)->get();
    // if($doctor->count() == 0){
    //     return redirect('/layanan')->with('status', 'tidak ada dokter dengan kategori diatas');   
    // }
    // else{
        // Session::put('category_doctor',$request->category_doctor);                  
        // foreach($doctor as $n){
            // $uu[] = [
                // 'id_doctor' => $n->id
            // ];
        // }   
        // $i=0;           
        // foreach($doctor as $n){    
        $data =DB::table('ruangan')
                ->join('room_used', 'ruangan.ruangan', '=', 'room_used.id')
                ->join('doctors','ruangan.doctor', '=', 'doctors.id')
                ->where('room_used.hari', '=' , $day->id)
                ->select( 'ruangan.*', 'room_used.*', 'doctors.*')                
                ->orderBy('room_used.hari', 'desc')
                ->get();
        // dd($data);
        
        $available = \App\room_used::where('hari',$day->id)->get();                  
        // $i=$i+1;
        // }                
        $patient = \App\patient::all();  
        $listregis = \App\service_register::all();
        // dd($doctor);        
        return view('register_patient/search',compact('data','day', 'patient', 'listregis'));
    
    }
    public function search(Request $request)
    {   
        $no=1;
        // dd($request);           
        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        
        $patient = \App\patient::where('name',$request->name)->where('number',$request->number)->get()->first();  
        if($patient == null){
            return redirect('/layanan_schedule')->with('status', 'pasien belum mendaftar');   
        }
        else
        {
            $service = \App\service_register::where('patient_id',$patient->id)->get();            
        }
       if($service->count() == 0){
        // $ruangan = \App\ruangan::get();                
        // $doctor = \App\doctor::where('category',Session('category_doctor'))->get('id');   
        // foreach($doctor as $n){
        //     $uu[] = [
        //         'id_doctor' => $n->id
        //     ];
        // }   
        // $i=0;           
        // foreach($doctor as $n){
        $patient = \App\patient::where('name',$request->name)->where('number',$request->number)->get();
           $data =DB::table('ruangan')
                ->join('room_used', 'ruangan.ruangan', '=', 'room_used.id')
                ->join('doctors','ruangan.doctor', '=', 'doctors.id')
                ->where('room_used.hari', '=' , $day->id)
                ->select( 'ruangan.*', 'room_used.*', 'doctors.*')                
                ->orderBy('room_used.hari', 'desc')
                ->get();
        // $i=$i+1;
        // }        
        return view('register_patient/show_patient',compact('data','day', 'patient','no'));
        }
        else{
        $available = \App\jadwal::where('hari',$day->id)->get();                  

         return redirect('/layanan_schedule')->with('status', 'pasien belum menyelesaikan administrasi sebelumnya');   
        }
    }

    


    // public function showR(Request $request)
    // {             
    //     // $day = Carbon::now('Asia/Jakarta')->format('D');
    //     $jadwal = \App\jadwal::where('hari',$request->day)->where('mulai',$request->mulai)->where('selesai',$request->selesai)->get();                
    //     $ruangan = \App\ruangan::get();

    //     // dd($day);
        
        
    //     return view('scheduledoctor/searchroom',compact( 'jadwal', 'ruangan'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
           $doctor = \App\doctor::find($id);
        return view('doctor/edit_doctor', compact('doctor'));
    }
    public function reset($id)
    {
        //
        \App\doctor::where('id',$id)
                    ->update([
                        'password'=>bcrypt('12345'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        return redirect('/doctor')->with('status', 'Reset password success');        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20|required',
                        'category'=> 'in:umum,Spesialis Bedah Mulut (Sp. BM),Spesialis Kedokteran Gigi Anak (Sp. KGA),Spesialis Konservasi Gigi (Sp. KG),Spesialis Penyakit Mulut (Sp. PM),Spesialis Ortodonsia (Sp. Ort),Spesialis Periodonsia (Sp. Perio),Spesialis Prostodonsia (Sp. Pros),Spesialis Radiologi Kedokteran Gigi (Sp. RKG)|required',                        
                        'place_of_birth'=>'required|string|max:20',
                        'date_of_birth'=>'required|date',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string|required',
                        'religion'=>'in:islam,hindu,budha,katolik,protestan,konghucu,lain-lain|required',                        
                        'identity_number'=>'string|max:100|required',        
                        'username'=>'required|string|max:100',                                    
                        ]);

        \App\doctor::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'place_of_birth'=>$request->place_of_birth,
                        'date_of_birth'=>$request->date_of_birth,
                        'gender'=>$request->gender,
                        'address'=>$request->address,
                        'religion'=>$request->religion,
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
                    ]);
        return redirect('/doctor')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \App\doctor::destroy($id);
    return redirect('/doctor')->with('status', 'Delete success');
    }
}
