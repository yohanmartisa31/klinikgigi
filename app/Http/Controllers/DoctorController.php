<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;


class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $doctor = \App\doctor::all();
        return view('doctor/home',['doctor' => $doctor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('doctor/register_doctor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20|required',
                        'category'=> 'in:umum,Spesialis Bedah Mulut (Sp. BM),Spesialis Kedokteran Gigi Anak (Sp. KGA),Spesialis Konservasi Gigi (Sp. KG),Spesialis Penyakit Mulut (Sp. PM),Spesialis Ortodonsia (Sp. Ort),Spesialis Periodonsia (Sp. Perio),Spesialis Prostodonsia (Sp. Pros),Spesialis Radiologi Kedokteran Gigi (Sp. RKG)|required',
                        'place_of_birth'=>'required|string|max:20',
                        'date_of_birth'=>'required|date',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string|required',
                        'religion'=>'in:islam,hindu,budha,katolik,protestan,konghucu,lain-lain|required',                        
                        'identity_number'=>'string|max:100|required',        
                        'username'=>'required|string|max:100',            
                        
                        ]);

        \App\doctor::create([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'place_of_birth'=>$request->place_of_birth,
                        'date_of_birth'=>$request->date_of_birth,
                        'gender'=>$request->gender,
                        'address'=>$request->address,
                        'religion'=>$request->religion,
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        $data =  new \App\ModelUser();
        $data->username = $request->username;
        $data->roll ='dokter';
        $data->password = bcrypt('123456');
        $data->remember_token = Str::random(60);
        $data->save();
         return redirect('/doctor')->with('status', 'Register success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
           $doctor = \App\doctor::find($id);
        return view('doctor/edit_doctor', compact('doctor'));
    }
    public function reset($id)
    {
        //
        \App\doctor::where('id',$id)
                    ->update([
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        $auth = \App\doctor::where('id',$id)->first();        
        \App\ModelUser::where('username',$auth->username)
                        ->update([
                            'password'=>bcrypt('123456'),    
                            'remember_token' => Str::random(60),
                        ]);
        return redirect('/doctor')->with('status', 'Reset password success');        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20|required',
                        'category'=> 'in:umum,Spesialis Bedah Mulut (Sp. BM),Spesialis Kedokteran Gigi Anak (Sp. KGA),Spesialis Konservasi Gigi (Sp. KG),Spesialis Penyakit Mulut (Sp. PM),Spesialis Ortodonsia (Sp. Ort),Spesialis Periodonsia (Sp. Perio),Spesialis Prostodonsia (Sp. Pros),Spesialis Radiologi Kedokteran Gigi (Sp. RKG)|required',                        
                        'place_of_birth'=>'required|string|max:20',
                        'date_of_birth'=>'required|date',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string|required',
                        'religion'=>'in:islam,hindu,budha,katolik,protestan,konghucu,lain-lain|required',                        
                        'identity_number'=>'string|max:100|required',        
                        'username'=>'required|string|max:100',                                    
                        ]);

        \App\doctor::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'place_of_birth'=>$request->place_of_birth,
                        'date_of_birth'=>$request->date_of_birth,
                        'gender'=>$request->gender,
                        'address'=>$request->address,
                        'religion'=>$request->religion,
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
                    ]);
        return redirect('/doctor')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \App\doctor::destroy($id);
    return redirect('/doctor')->with('status', 'Delete success');
    }
}
