<?php

namespace App\Http\Controllers;
​
use Illuminate\Http\Request;
use Excel;
use App\Exports\UserReport;
​
class UserController extends Controller
{    
    public function laporanExcel()
    {
        return (new UserReport)->download('users.xlsx');
    }
}