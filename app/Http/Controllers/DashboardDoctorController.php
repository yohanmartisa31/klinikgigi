<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

use Auth;

class DashboardDoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $user = Auth::user()->username;
            $task = \App\service_register::all(); 
            $doctor = \App\doctor::where('username',Session('username'))->get()->first();
            
            // dd($doctor->id);
            // $jadwal = \App\jadwal::where('doctor',$doctor->id)->get();
            // foreach($jadwal as $n){
            // $uu[] = [
            //     'id_doctor' => $n->id
            //     ];
            // }
            // $i=0;           
            // foreach($jadwal as $n){
                // $available[$i] = \App\jadwal::where('hari',$day->id)->where('ready','0')->where('doctor',$uu[$i])->first();                  
                $listpatient = \App\service_register::where('id_doctor',$doctor->id)->where('cek','0')->get();                
            // $i=$i+1;
            // }        
            // dd($jadwal);            
            $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');
            Session::put('date',$date);
            // dd($user);
        // $task = \App\ModelUser::find($user);
        // return view('dashboardoctor/home',['task' => $task])
            return view('dashboarddoctor/home', compact('user','listpatient','doctor','date'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->note);
        // return request('practice');
        // $aa = json_decode($request->practice, true);
        // dd($aa);
        // return json_decode($request, true);        
        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
        
        // dd(json_decode($request->practice, true));
        // $medical = new \App\medicalrecord();// $medical->name = request('name');
        // $medical->complaint = request('complaint');
        // $medical->diagnosa = request('diagnosa');
        // $medical->note = request('note');
        // $medical->patient_id = request('patient_id');        
        // $medical->practice = json_encode($request->practice);
        // dd(json_decode($request->practice));
        $i=0;
        $harga2 =0;
        foreach ($request->practice  as $key) {
            # code...

        $jenispelayanan[$key] = \App\practice::where('id',$request->practice[$i])->get('name_practice')->first();
        $harga[$i] = \App\practice::where('id',$request->practice[$i])->get('price')->first();
        $ha[$i] =  DB::table('practices')
                ->join('price','practices.price','=', 'price.id')                                
                ->where('practices.price','=',$harga[$i]->price)
                ->select('price.price')                                
                ->get('price');
        $harga2 += $ha[$i]->first()->price;
        $i= $i+1;
        // // $data[$key] = $jenispelayanan[$key]->price;
        }
                // ->join('price', 'practices.price', '=', 'price.id')                                        
        $s=json_encode($jenispelayanan);
        $doctor = \App\doctor::where('username',Session('username'))->get()->first();
        $patient = \App\patient::where('id',Session('id_patient'))->get()->first();
        $perawat = \App\employee::where('id',$request->perawat)->get()->first();
        
        // dd($request->practice[0]);
        
        \App\catatan_pemeriksaan::create([
                            'id_service_register'=>Session('id_service_register'),
                            'id_employee'=> $request->perawat,
                            'id_doctor'=>Session('id_doctor'),
                            'id_jenis_pelayanan' => json_encode($request->practice),
                            'no_pendaftaran' => Session('no_pendaftaran'),
                            'diagnosa' => $request->diagnosa,
                            'catatan' => $request->note,
                            'complaint' => Session('complaint'),                            
                            'nama_jenis_pelayanan' => $s,
                            'price' => $harga2,
                            'nama_dokter' => $doctor->name,                            
                            'nama_pasien' => $patient->name,
                            'nama_perawat' => $perawat->name,                            
                            'tanggal'=>Session('date'),      
                            
            ]);
          //  bigint(20) UNSIGNED NOT NULL,
          
          
          
                    
          
           
        \App\service_register::where('id_doctor',Session('id_doctor'))
                        ->update([
                            'cek'=> '1',
                        ]);

        // \App\jadwal::where('hari',$day->id)->where('doctor',Session('id_doctor'))
        //                 ->update([
        //                     'ready'=>'0',
        //                 ]);
        // $hapus =\App\service_register::where('patient_id',Session('id_patient'))->where('jadwal',Session('jadwal'))->first();

        //hapus no antrian
        // \App\service_register::destroy(Session('id'));
     
        // dd($practice);

        // $medical->save();

        // $medical2 = \App\medicalrecord::find($request);
        // dd($medical2);
        // \App\medicalrecord::create($request->all());
        // \App\service_register::destroy($medical->patient_id);
         return redirect('/dashboarddoctor')->with('status', 'Register success');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function edit($id)
    {
        //
        $doctor = \App\doctor::where('username',Session('username'))->get()->first();
        $employee = \App\employee::where('category','perawat')->get();
        $practice = \App\practice::where('category_doctor',$doctor->category)->get();
        $task = \App\service_register::find($id);

        // dd($doctor->category_doctor);
        Session::put('no_pendaftaran',$task->no_pendaftaran);
        Session::put('id_service_register',$task->id);
        Session::put('id_patient',$task->patient_id);
        Session::put('complaint',$task->complaint);
        Session::put('id_doctor',$doctor->id);
        
        Session::put('id',$id);
        return view('dashboarddoctor/form_patient',compact('task', 'practice', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
