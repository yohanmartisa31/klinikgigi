<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;


class ScheduleDoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $doctor = \App\doctor::all();
        return view('scheduledoctor/home',['doctor' => $doctor]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('doctor/register_doctor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
           // $jadwal = \App\room_used::where('ruangan',$request->room)->where('hari',$request->day)->get()->first();      
                   
           // $doktor_ready= \App\ruangan::where('doctor',$request->name)->get()->first();
       
           
           // // if($doktor_ready != null ){
           // //      return redirect('/schedule_doctor')->with('status', 'gagal ruangan terpakai dan dokter telah terdaftar di hari itu');   
           // // }           
           // // if($jadwal != null){
           // //      return redirect('/schedule_doctor')->with('status', 'gagal ruangan terpakai');   
           // // }
           // if($doktor_ready != null){
           //      return redirect('/schedule_doctor')->with('status', 'dokter telah terdaftar di hari itu');   
           // }
           // else{
        $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::where('kode_hari', $day)->first();
            $dokter =  DB::table('ruangan')
                ->join('room_used','ruangan.ruangan','=', 'room_used.id')                                         
                ->select('room_used.*','ruangan.*')                                
                ->where('ruangan.doctor','=', $request->name)                
                ->where('room_used.hari','=',$request->day)
                ->get();
            $ruangan =  DB::table('ruangan')
                ->join('room_used','ruangan.ruangan','=', 'room_used.id')                                         
                ->select('room_used.*','ruangan.*')                                     
                ->where('room_used.hari','=',$request->day)
                ->where('room_used.ruangan','=',$request->room)
                ->get();
        if($dokter->count() != null){
            return redirect('/schedule_doctor')->with('status', 'dokter telah terdaftar di hari itu');   
        }
        else{
            $i=0;                        
          if($ruangan->count() != null){ 
            foreach ($ruangan as $key) {
                $mulai= substr($ruangan[$i]->mulai,0,5);
                $selesai= substr($ruangan[$i]->selesai,0,5);
                if(($request->mulai>=$mulai)&&($request->selesai<=$selesai))
                {
                    return redirect('/schedule_doctor')->with('status', 'gagal ruangan terpakai');   
                }                
                elseif(($request->mulai < $mulai)&&($request->selesai < $mulai))
                {
                  \App\room_used::create([
                    'hari' => $request->day,
                    'ruangan' => $request->room,
                    'mulai' => $request->mulai,
                    'selesai'=>$request->selesai,  
                  ]);
                  $ruangan= \App\room_used::where('ruangan',$request->room)->where('hari',$request->day)->where('mulai',$request->mulai)->where('selesai',$request->selesai)->get()->first();
                  Session::put('doctor',$request->name);
                  Session::put('ruangan',$ruangan->id);           
                  \App\ruangan::create([
                    'doctor' => Session('doctor'),                            
                    'ruangan'=>Session('ruangan'),                            
                    'ready' =>'0',                      
                  ]);
                    return redirect('/schedule_doctor')->with('status', 'Register success');
                }
                elseif(($request->mulai > $selesai)&&($request->selesai > $selesai))
                {
                  \App\room_used::create([
                    'hari' => $request->day,
                    'ruangan' => $request->room,
                    'mulai' => $request->mulai,
                    'selesai'=>$request->selesai,  
                  ]);
                  $ruangan= \App\room_used::where('ruangan',$request->room)->where('hari',$request->day)->where('mulai',$request->mulai)->where('selesai',$request->selesai)->get()->first();
                  Session::put('doctor',$request->name);
                  Session::put('ruangan',$ruangan->id);           
                  \App\ruangan::create([
                    'doctor' => Session('doctor'),                            
                    'ruangan'=>Session('ruangan'),                            
                    'ready' =>'0',                      
                  ]);
                    return redirect('/schedule_doctor')->with('status', 'Register success');                
                }
                else{                    
                    return redirect('/schedule_doctor')->with('status', 'gagal ruangan masih terpakai');                
                }
                    $i++;
            }
          } 
          else{
             \App\room_used::create([
                    'hari' => $request->day,
                    'ruangan' => $request->room,
                    'mulai' => $request->mulai,
                    'selesai'=>$request->selesai,  
                  ]);
                  $ruangan= \App\room_used::where('ruangan',$request->room)->where('hari',$request->day)->get()->last();
                  Session::put('doctor',$request->name);
                  Session::put('ruangan',$ruangan->id);           
                  \App\ruangan::create([
                    'doctor' => Session('doctor'),                            
                    'ruangan'=>Session('ruangan'),                            
                    'ready' =>'0',                      
                  ]);
                    return redirect('/schedule_doctor')->with('status', 'Register success'); 
          } 
        }
        
           
        }
           // dd($ruangan->id);    
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // $day = Carbon::now('Asia/Jakarta')->format('D');
        $day = \App\hari::get();
        $room_used = \App\listruangan::get();

        // dd($day);
        $doctor = \App\doctor::where('category',$request->category_doctor)->get();        
        $category=$request->category_doctor;
        return view('scheduledoctor/search',compact('doctor','day', 'category', 'room_used'));
    }
    public function room(Request $request)
    {
        Session::put('day',$request->day);
        Session::put('name',$request->name);
        Session::put('mulai',$request->mulai);
        Session::put('selesai',$request->selesai);        
        $room_used = \App\room_used::get();
        return view('scheduledoctor/room',compact('room_used'));
    }
    // public function showR(Request $request)
    // {             
    //     // $day = Carbon::now('Asia/Jakarta')->format('D');
    //     $jadwal = \App\jadwal::where('hari',$request->day)->where('mulai',$request->mulai)->where('selesai',$request->selesai)->get();                
    //     $ruangan = \App\ruangan::get();

    //     // dd($day);
        
        
    //     return view('scheduledoctor/searchroom',compact( 'jadwal', 'ruangan'));
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
           $doctor = \App\doctor::find($id);
        return view('doctor/edit_doctor', compact('doctor'));
    }
    public function reset($id)
    {
        //
        \App\doctor::where('id',$id)
                    ->update([
                        'password'=>bcrypt('12345'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        return redirect('/doctor')->with('status', 'Reset password success');        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20|required',
                        'category'=> 'in:umum,Spesialis Bedah Mulut (Sp. BM),Spesialis Kedokteran Gigi Anak (Sp. KGA),Spesialis Konservasi Gigi (Sp. KG),Spesialis Penyakit Mulut (Sp. PM),Spesialis Ortodonsia (Sp. Ort),Spesialis Periodonsia (Sp. Perio),Spesialis Prostodonsia (Sp. Pros),Spesialis Radiologi Kedokteran Gigi (Sp. RKG)|required',                        
                        'place_of_birth'=>'required|string|max:20',
                        'date_of_birth'=>'required|date',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string|required',
                        'religion'=>'in:islam,hindu,budha,katolik,protestan,konghucu,lain-lain|required',                        
                        'identity_number'=>'string|max:100|required',        
                        'username'=>'required|string|max:100',                                    
                        ]);

        \App\doctor::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'place_of_birth'=>$request->place_of_birth,
                        'date_of_birth'=>$request->date_of_birth,
                        'gender'=>$request->gender,
                        'address'=>$request->address,
                        'religion'=>$request->religion,
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
                    ]);
        return redirect('/doctor')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \App\doctor::destroy($id);
    return redirect('/doctor')->with('status', 'Delete success');
    }
}
