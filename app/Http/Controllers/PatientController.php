<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $patient = DB::table('patient')->get();
        $patient = \App\patient::all();
        
        return view('patient/home',['patient' => $patient]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient/register_patient');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20|required',           
                        'place_of_birth'=>'required|string|max:20',
                        'date_of_birth'=>'required|date',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string|required',
                        'religion'=>'in:islam,hindu,budha,katolik,protestan,konghucu,lain-lain|required',                        
                        'identity_number'=>'string|max:100|required',        
                        'job'=>'required|string',
                        'citizenship'=> 'required|string',
                        
        ]);                
         \App\patient::create($request->all());
         return redirect('/patient')->with('status', 'Register success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $patient = \App\patient::find($id);
        return view('patient/edit_patient', compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'name' => 'required',
          'number' => 'required|max:20',
        ]);
        \App\patient::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'number' => $request->number
                    ]);
        return redirect('/patient')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_patient)
    {
    \App\patient::destroy($id_patient);
    return redirect('/patient')->with('status', 'Delete success');
    }
}
