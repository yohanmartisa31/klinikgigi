<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    //ini yang bawah satu baris hapus aja soalnya di urang ribet mesti migrate ulang
    protected $table = 'transaksi';
    protected $cast = [
 	  'practice' => 'array'
    ];        
    protected $fillable = ['id_catatan_pemeriksaan', 'id_karyawan', 'tanggal', 'harga','nama_front_office', 'kategori_transaksi', 'cara_pembayaran','produk_tambahan','status', 'nama_pasien', 'nama_perawat', 'nama_jenis_pelayanan'];          
}
