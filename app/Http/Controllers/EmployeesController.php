<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;


class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employee = \App\employee::all();
        return view('employees/home',['employee' => $employee]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('employees/register_employees');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $this->validate($request, [
                        'name' => 'required|string|max:100',
                        'number' => 'string|max:20',
                        'category'=> 'required',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string',                        
                        'identity_number'=>'string|max:100',        
                        'username'=>'required|string|max:100',                                    
                        ]);

        \App\employee::create([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'gender'=>$request->gender,
                        'address'=>$request->address,                        
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        if($request->category == 'admin'||'fo'){
        $data =  new \App\ModelUser();
        $data->username = $request->username;
        $data->roll = $request->category;
        $data->password = bcrypt('123456');
        $data->remember_token = Str::random(60);
        $data->save();
                        
        
        }
         return redirect('/employees')->with('status', 'Register success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
           $employees = \App\employee::find($id);
        return view('employees/edit_employees', compact('employees'));
    }
    public function reset($id)
    {
        //
        \App\employee::where('id',$id)
                    ->update([
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),
        ]);
        $auth = \App\employee::where('id',$id)->first();        
        \App\ModelUser::where('username',$auth->username)
                        ->update([
                            'password'=>bcrypt('123456'),    
                        ]);
        return redirect('/employees')->with('status', 'Reset password success');        
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
                         'name' => 'required|string|max:100',
                        'number' => 'string|max:20',
                        'category'=> 'required',
                        'gender'=>'in:PRIA,WANITA|required',
                        'address'=>'string',                        
                        'identity_number'=>'string|max:100',        
                        'username'=>'required|string|max:100',                                                            
                        ]);

        \App\employee::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'number' => $request->number,
                        'category'=>$request->category,
                        'gender'=>$request->gender,
                        'address'=>$request->address,                        
                        'identity_number'=>$request->identity_number,        
                        'username'=>$request->username,            
                        'password'=>bcrypt('123456'),    
                        'pass_reset_date'=>Carbon::now('Asia/Jakarta'),
                        'reset_by'=>Session('username'),             ]);
        return redirect('/employees')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        \App\employee::destroy($id);
    return redirect('/employees')->with('status', 'Delete success');
    }
}
