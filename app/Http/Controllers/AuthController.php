<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;


class AuthController extends Controller
{
    public function index()
    {
        $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        
        $tanggalini=\App\tanggalini::get()->first();
        if($tanggalini == null){
            \App\tanggalini::create([
              'tanggal' => $date,              
              ]);              
        }
        elseif($tanggalini->tanggal != $date){
                $hapus = \App\service_register::where('tanggal',$tanggalini->tanggal)->get()->count();                
                if($hapus != 0){
                $hapusdata =  DB::table('service_register')                
                        ->where('service_register.tanggal','=',$tanggalini->tanggal)
                        ->delete();                
                }                
                
                \App\tanggalini::where('tanggal',$tanggalini->tanggal)
                                ->update([
                                 'tanggal' => $date,              
                                ]);                              
        }                                   
    	return view('login');
    }
    public function login(Request $request)
    {
        $patient = \App\patient::get()->count();
        $date = Carbon::now('Asia/Jakarta')->format('Y-m-d');
        $request->validate([
          'username' => 'required',
          'password' => 'required',
        ]);        
    	if(Auth::attempt($request->only('username','password'))){          
            Session::put('username',$request->username);     
            Session::put('date',$date);                              
            Session::put('patient',$patient);                              
    		return redirect('/')->with('status','Kamu berhasil Login');
    	}
    	return redirect('/login')->with('status','kamu gagal login');
    }

    public function register()
    {
    	return view('register');
    }
    public function registerUser(Request $request)
    {    
    	    	// dd($request->all());
	
    	$this->validate($request, [
            'username' => 'required|min:4|unique:auths',
            'roll' => 'required',
            'password' => 'required',         
        ]);

        $data =  new \App\ModelUser();
        $data->username = $request->username;
        $data->roll = $request->roll;
        $data->password = bcrypt($request->password);
        $data->remember_token = Str::random(60);
        $data->save();
        return redirect('/login')->with('status','Kamu berhasil Register');
    }
    public function logout(){
        Auth::logout();
        session()->forget('masuk');
        session()->flush();
        return redirect('/login');
    }       
}
