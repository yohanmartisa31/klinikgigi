<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

use PDF;
use Excel;
use App\Exports\UserReport;
use App\Exports\UserReportklinik;


class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        // $doctor2 =\App\doctor::find($doctor)->collection->id;
        // $doctor2 = DB::table('doctor')->get();
        // dd($doctor2);
        return view('report/index');
        // ['patient' => $patient]
        // 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
      */
    public function createsession(Request $request, $id)
    {
        $request->session()->put('patient_id', $request->patient_id);
        $request->session()->put('category', $request->category);
        $request->session()->put('name', $request->name);
        $request->session()->put('complaint', $request->complaint);
        $patient_id = session()->get('patient_id');
        $category = session()->get('category');
        $name = session()->get('name');
        $complaint = session()->get('complaint');
        $doctor =\App\doctor::all();
        return view('/service/selectdoctor', compact('patient_id', 'category', 'name', 'complaint', 'doctor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {     
        //
        // $queue =DB::table('users')->where('patient_id', '$id');
        // $service =\App\service_register::orderBy('id', 'DESC')->take(1)->latest();
        \App\service_register::create($request->all());
                          
        // $all=\App\service_register::all();
    
         // return view('/service/print_form', compact('no'));
         return redirect('/patient/printform');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        $dokter =\App\catatan_pemeriksaan::where('nama_dokter', $request->name)->count();
        $perawat = \App\transaksi::where('nama_perawat', $request->name)->count();
        if($dokter != null){
        $name =  \App\catatan_pemeriksaan::where('nama_dokter', $request->name)->get()->first()->nama_dokter;  
        $category = \App\doctor::where('name',$request->name)->get()->first()->category;
        $filter = \App\catatan_pemeriksaan::where('nama_dokter', $request->name)->get();
        
        $transaksi =DB::table('transaksi')
                ->join('catatan_pemeriksaan', 'transaksi.id_catatan_pemeriksaan', '=', 'catatan_pemeriksaan.id')
                ->where('catatan_pemeriksaan.nama_dokter', '=' , $request->name)                
                ->select( 'transaksi.*','catatan_pemeriksaan.*')                                
                ->get();
        // dd($filter);
        }

        elseif($perawat != null){
         $name =  \App\transaksi::where('nama_perawat', $request->name)->get()->first()->nama_perawat;     
        $category = \App\employee::where('name',$request->name)->get()->first()->category;
            $transaksi = \App\transaksi::where('nama_perawat',$request->name)->get();
        }else{
            $name =  \App\transaksi::where('nama_front_office', $request->name)->get()->first()->nama_front_office;     
            $category = \App\employee::where('name',$request->name)->get()->first()->category;
            $transaksi = \App\transaksi::where('nama_front_office',$request->name)->get();
        }                
        $mulai=$request->tgl_mulai;
        $selesai=$request->tgl_selesai;
        Session::put('name',$name);
        Session::put('category',$category);
        Session::put('transaksi',$transaksi);        
        Session::put('mulai',$mulai);
        Session::put('selesai',$selesai);
        // $pdf = PDF::loadview('/service/antrian_pdf',compact('name','category','transaksi','mulai','selesai'));
        // return $pdf->stream();    
        return view('report/download');
    }

    public function showklinik(Request $request)
    {
        
       
        
    
        
        $transaksi =DB::table('transaksi')
                ->join('catatan_pemeriksaan', 'transaksi.id_catatan_pemeriksaan', '=', 'catatan_pemeriksaan.id')   
                ->select( 'transaksi.*','catatan_pemeriksaan.*')                                
                ->get();
        // dd($filter);        
        $mulai=$request->tgl_mulai;
        $selesai=$request->tgl_selesai;
        
        Session::put('transaksi',$transaksi);        
        Session::put('mulai',$mulai);
        Session::put('selesai',$selesai);
        // $pdf = PDF::loadview('/service/antrian_pdf',compact('name','category','transaksi','mulai','selesai'));
        // return $pdf->stream();    
        return view('report/downloadklinik');
    }
    public function cetak_pdfklinik()
    {
        
        $pdf = PDF::loadview('/service/antrian_pdfklinik');
        return $pdf->stream();
    }

    public function export_excelklinik()
    {
        ob_end_clean(); 
        ob_start();
        return Excel::download(new UserReportklinik(),'usersklinik.xlsx');                
    }
    public function cetak_pdf()
    {
        
        $pdf = PDF::loadview('/service/antrian_pdf');
        return $pdf->stream();
    }

    public function export_excel()
    {
        ob_end_clean(); 
        ob_start();
        return Excel::download(new UserReport(),'users.xlsx');                
    }
    /**

     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}