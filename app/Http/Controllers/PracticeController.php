<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class PracticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $practice = \App\practice::all();
        return view('practice/home',['practice' => $practice]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('practice/register_practice');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
                        'name' => 'required|string|max:100',                        
                        'category_doctor'=> 'in:umum,Spesialis Bedah Mulut (Sp. BM),Spesialis Kedokteran Gigi Anak (Sp. KGA),Spesialis Konservasi Gigi (Sp. KG),Spesialis Penyakit Mulut (Sp. PM),Spesialis Ortodonsia (Sp. Ort),Spesialis Periodonsia (Sp. Perio),Spesialis Prostodonsia (Sp. Pros),Spesialis Radiologi Kedokteran Gigi (Sp. RKG)|required',                        
                        'price'=>'required',                                                
                        ]);
        

        \App\price::create([
                        'name_practice' => $request->name,                                
                        'category_doctor'=>$request->category_doctor,                      
                        'price' => $request->price,                        
                        'tanggal_update_harga' => Carbon::now('Asia/Jakarta'),                        
        ]);
        $id=\App\price::where('name_practice',$request->name)->where('category_doctor',$request->category_doctor)->orderBy('price.id', 'desc')->first();                
        
        $practice = \App\practice::create([
                        'name_practice' => $request->name,                        
                        'category_doctor'=>$request->category_doctor,                      
                        'price' => $id->id,
        ]);      

         return redirect('/practice')->with('status', 'Register success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $practice = \App\practice::find($id);               
        return view('practice/edit_practice', compact('practice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //    $request->validate([
        //   'name' => 'required',
        //   'number' => 'required|max:20',
        // ]);
        \App\price::create([
                        'name_practice' => $request->name_practice,                                
                        'category_doctor'=>$request->category_doctor,                      
                        'price' => $request->price,                        
                        'tanggal_update_harga' => Carbon::now('Asia/Jakarta'),                        
        ]);
        $id=\App\price::where('name_practice',$request->name_practice)->where('category_doctor',$request->category_doctor)->orderBy('price.id', 'desc')->first();                
        $practice = \App\practice::where('name_practice',$request->name_practice)
                                ->where('category_doctor',$request->category_doctor)
                                ->update([                        
                                'price' => $id->id,
        ]);      
        return redirect('/practice')->with('status', 'Update success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    \App\practice::destroy($id);
    return redirect('/practice')->with('status', 'Delete success');
    }
}
