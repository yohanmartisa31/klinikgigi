<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class listruangan extends Model 
{
    protected $table = 'listruangan';
    public $timestamps = true;
    protected $fillable = ['nama'];    
}