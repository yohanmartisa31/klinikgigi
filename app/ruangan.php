<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ruangan extends Model 
{

    protected $table = 'ruangan';
    public $timestamps = true;
    protected $fillable = array( 'doctor', 'ruangan', 'ready');

    
    public function getDoctor()
    {
    	    return $this->belongsTo('App\doctor', 'doctor');
	}
}