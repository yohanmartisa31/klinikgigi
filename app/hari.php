<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hari extends Model 
{

    protected $table = 'hari';
    public $timestamps = true;
    protected $fillable = array('kode_hari', 'nama_hari');

}