<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class room_used extends Model 
{

    protected $table = 'room_used';
    public $timestamps = true;
    protected $fillable = ['ruangan' ,'hari','mulai','selesai'];

    public function getHari()
    {
        return $this->belongsTo('App\hari', 'hari');
    }
    public function getRuangan()
    {
        return $this->belongsTo('App\ruangan', 'id');
    }
}