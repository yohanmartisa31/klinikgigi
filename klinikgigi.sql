-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 14 Sep 2020 pada 23.10
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinikgigi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `auths`
--

CREATE TABLE `auths` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roll` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `auths`
--

INSERT INTO `auths` (`id`, `username`, `password`, `roll`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dokter', '$2y$10$J0InBxRJnUy/jSb8W2K2x.M7lA9I0QFmDY/79JlhHjUsBOhUMQZt.', 'dokter', 'MdS5STkOhfQvi3pfCPsJqiUBwOBFtuYuI4eSNEBBWPX0m4kkBnjVAkW58RvB', '2020-05-25 18:07:22', '2020-05-25 18:07:22'),
(2, 'front', '$2y$10$jluuc4KZW02Bgvz0B9C6keKlWj8D4zPhGc8heQiLXLt4mjEHSn3cy', 'fo', 'gjMz8NtK3dN4RLRLgj6fPqAiAy3CUqL7nomS0FBlfuscZDEFge9xNiJi8ajw', '2020-05-25 18:07:51', '2020-09-01 16:58:56'),
(3, 'admin', '$2y$10$nirYBe3hx4u5BjtseRLvHOOwAsfwYi9/unVoKbs2zKgpxAJVLuv46', 'admin', '1tdrRGxeU2RVDK7BugWKTqITFuRyE5s8Dv4wGasLmIBDD6cejMooPNa5pLcS', '2020-05-25 18:08:04', '2020-05-25 18:08:04'),
(4, 'admin2', '$2y$10$WmMxS9TjTmUN/WURGFQu8.u1XuCCeKY2SFSZpR09GOuW7mLAxYfZm', 'admin', 'N6VsYkrtToKxXyQZPWUlB4S0rhoP46Zv0NGXZRpIYXDYtn4fr4d7q2lfmTe5', '2020-05-25 18:10:01', '2020-05-25 18:10:01'),
(5, 'gilang', '$2y$10$lh/SbwKZ215N.OjwWOYCauqhFwXWS7sg2bnUXzMQuEfaNYlhpQxje', 'fo', '74RZ4ZxW0xxZjYB15jut91prvDEvVbYI2sB5jYN2JifRTAkrzHU9bGA4iyMx', '2020-09-01 16:54:45', '2020-09-09 05:22:10'),
(6, 'yoni', '$2y$10$lgN1pQRJa6rWHsZMsFFv1ujqTMHiYsLeLgy/lP/2u1UD.5ywsUR6a', 'dokter', 'pQBRnW7HZrdCONp9pwmaeoPftmkM2jSHHTYnqfQbA6fpZc1kAljIq9wIJqlD', NULL, '2020-09-03 15:59:30'),
(7, 'yohan', '$2y$10$X8h2gamOfzZenozON7mWSejXk7qeYT30/UGe0Zeg1JJy8DQX0BNrW', 'dokter', 'otaK2BeQIXsKD5p1akxyFlfldyxcHoqDl48HkAi2fyeIsXCmEbdmmSELmwnf', NULL, '2020-09-09 09:14:27'),
(8, 'rindu', '$2y$10$mJ36ydgq8Qk25TJ/xuClFOU0ZP3o67T.wSJVVzs8KQC.ymHhBD3Da', 'dokter', 'vXujI0hKF92aKd3aCxgJInXBu5OCx49IcKvbfZtXEZJZYSs7cixfd4LxPa9W', NULL, '2020-09-04 04:42:09'),
(9, 'mei', '$2y$10$vwNSYLZqIZi7nLmLPcUvVO1/eg/vmM6a.ajnLh/zKDMmqk4AP.1G2', 'perawat', 'Ex2UGU9fFx9uarVCF34KwtmZ3bDTtrXP6dmINSz3Mn7pqpwtkNxU7sn5A7F8', '2020-09-07 18:14:27', '2020-09-07 18:14:27'),
(10, 'budi', '$2y$10$rPWJ4TDxoVbKuxIY04k7Ju1iryKeXV9SZIaQaPEsgIimbKByDtzXa', 'fo', 'bUFAeLmqxwRB08u8vmRAPUQqOsT6VbldudzI276nOTZa2lrojZxyhLRiTXBj', '2020-09-08 16:33:51', '2020-09-08 16:34:02'),
(11, 'budiman', '$2y$10$oWCuX5J2.AAmYB.08F.UD.E./tWQvxp2E50n8xrgvT5GFMViJILp6', 'dokter', 'v0qrqYTpYTq4xlGjm97kux2m1fI0UXG03MntbxZJR724016Bn40gOC2Wb1SI', '2020-09-08 16:35:40', '2020-09-08 16:35:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan_pemeriksaan`
--

CREATE TABLE `catatan_pemeriksaan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_service_register` bigint(20) UNSIGNED NOT NULL,
  `id_jenis_pelayanan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_employee` bigint(20) UNSIGNED NOT NULL,
  `id_doctor` bigint(20) UNSIGNED NOT NULL,
  `nama_jenis_pelayanan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_dokter` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_pasien` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_perawat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diagnosa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date DEFAULT NULL,
  `no_pendaftaran` int(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `catatan_pemeriksaan`
--

INSERT INTO `catatan_pemeriksaan` (`id`, `id_service_register`, `id_jenis_pelayanan`, `id_employee`, `id_doctor`, `nama_jenis_pelayanan`, `nama_dokter`, `nama_pasien`, `nama_perawat`, `price`, `diagnosa`, `catatan`, `complaint`, `tanggal`, `no_pendaftaran`, `created_at`, `updated_at`) VALUES
(1, 10, '[\"4\",\"5\",\"6\"]', 9, 4, '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"},\"6\":{\"name_practice\":\"pemasangan kawat gigi\"}}', 'yoni', 'gile', 'mei', '850000', 'pergeseran posisi gigi yang tidak wajar', 'sakit jiwa ini mah', 'sakit gigi', '2020-09-08', 1, '2020-09-08 22:01:27', '2020-09-07 22:01:27'),
(2, 11, '[\"4\",\"5\"]', 9, 2, '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', 'yohan martisa', 'dudi', 'mei', '350000', 'sakit gigi', 'cek up lagi sebulan sekali', 'sakit gigi', '2020-09-08', 2, '2020-09-08 16:57:54', '2020-09-08 16:57:54'),
(3, 12, '[\"4\",\"6\"]', 9, 2, '{\"4\":{\"name_practice\":\"tambal laser\"},\"6\":{\"name_practice\":\"pemasangan kawat gigi\"}}', 'yohan martisa', 'wilda', 'mei', '700000', 'pergeseran posisi gigi yang tidak wajar', 'minum obat pereda nyeri', 'sakit gigi', '2020-09-08', 3, '2020-09-08 20:49:55', '2020-09-08 20:49:55'),
(4, 13, '[\"5\"]', 9, 2, '{\"5\":{\"name_practice\":\"scalling\"}}', 'yohan martisa', 'rindu', 'mei', '150000', 'sakit', '1 bulan melakukan checkup kembali', 'karang gigi', '2020-09-08', 4, '2020-09-08 21:45:42', '2020-09-08 21:45:42'),
(5, 15, '[\"4\",\"5\"]', 9, 4, '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', 'yoni', 'prabowo', 'mei', '350000', 'bolong', '1 bulan melakukan checkup kembali', 'sakit gigi', '2020-09-09', 1, '2020-09-09 05:36:15', '2020-09-09 05:36:15'),
(6, 16, '[\"4\",\"5\"]', 9, 4, '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', 'yoni', 'wilda', 'mei', '350000', 'tambal gigi', '1 bulan melakukan checkup kembali', 'sakit gigi', '2020-09-09', 2, '2020-09-09 09:22:16', '2020-09-09 09:22:16'),
(7, 17, '[\"4\",\"5\"]', 9, 2, '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', 'yohan martisa', 'wilda', 'mei', '350000', 'pergeseran posisi gigi yang tidak wajar', '1 bulan melakukan checkup kembali', 'sakit gigi', '2020-09-11', 1, '2020-09-11 17:41:28', '2020-09-11 17:41:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_doctor`
--

CREATE TABLE `category_doctor` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `category_doctor`
--

INSERT INTO `category_doctor` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'umum', NULL, NULL),
(2, 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', NULL, NULL),
(3, 'Spesialis Bedah Mulut (Sp. BM)', NULL, NULL),
(4, 'Spesialis Radiologi Kedokteran Gigi (Sp. RKG)', NULL, NULL),
(5, 'Spesialis Konservasi Gigi (Sp. KG)', NULL, NULL),
(6, 'Spesialis Penyakit Mulut (Sp. PM)', NULL, NULL),
(7, 'Spesialis Ortodonsia (Sp. Ort)', NULL, NULL),
(8, 'Spesialis Periodonsia (Sp. Perio)', NULL, NULL),
(9, 'Spesialis Prostodonsia (Sp. Pros)', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `doctors`
--

CREATE TABLE `doctors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_reset_date` timestamp NULL DEFAULT NULL,
  `reset_by` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `category`, `place_of_birth`, `date_of_birth`, `gender`, `address`, `religion`, `number`, `identity_number`, `username`, `password`, `pass_reset_date`, `reset_by`, `created_at`, `updated_at`) VALUES
(2, 'yohan martisa', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', 'tangerang', '1997-03-31', 'PRIA', 'jln mangarai kelurahan mangarai kec jakarta selatan, banten ,indonesia', 'islam', '0899877788', '320320938493', 'yohan', '$2y$10$doKJpAWlwM2hD6ZGNTFKseA9hfI81gmppVGlTn1imcV/u9LCkZhPe', '2020-09-09 16:14:27', 'admin', '2020-09-01 00:38:56', '2020-09-09 09:14:27'),
(3, 'rindu', 'Spesialis Konservasi Gigi (Sp. KG)', 'bandung', '1970-02-01', 'WANITA', 'JL SUKASARI 1 NO 56 RT/RW 002/001 KELURAHAN SEKELOA KECAMATAN COBLONG', 'hindu', '08393938839', '320320193738828', 'rindu', '$2y$10$iJbqNJxmZbt9krGKTa3o/OjC5PTuWYKF29gNNceCkWb2Zm82arfSG', '2020-09-04 11:42:09', 'admin', '2020-09-01 00:48:31', '2020-09-04 04:42:09'),
(4, 'yoni', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', 'tangerang', '1997-03-31', 'PRIA', 'jakarta blok a no 3 kelapa gading', 'islam', '086466363636', '320320938883738', 'yoni', '$2y$10$fVFL96woriHJVXoR.2ozzOzHupaZ41qiwQzyNx8fDmJvbkJPuu6QW', '2020-09-03 22:59:30', 'admin', '2020-09-02 01:40:10', '2020-09-03 15:59:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('fo','perawat','officeboy','admin') COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_reset_date` timestamp NULL DEFAULT NULL,
  `reset_by` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `employees`
--

INSERT INTO `employees` (`id`, `name`, `category`, `gender`, `address`, `number`, `identity_number`, `username`, `password`, `pass_reset_date`, `reset_by`, `created_at`, `updated_at`) VALUES
(2, 'gilang', 'fo', 'PRIA', 'tangerang', '08874211323', '3203203948432', 'gilang', '$2y$10$dzbMQRTrYrCCWUG08L80gOHHSYmd1jkxFGREocEYYEOV4YaDNoeLm', '2020-09-09 12:22:09', 'admin', '2020-09-01 16:50:37', '2020-09-09 05:22:09'),
(9, 'mei', 'perawat', 'WANITA', 'jakarta blok a no 3 kelapa gading', '081111111', '320320330011', 'mei', '$2y$10$uA2V4ETuEUhyMJQTynXQYOtjGxgaVMU7jpwdZPuAZhqwY3FIAkzuS', '2020-09-08 01:14:26', 'admin', '2020-09-07 18:14:26', '2020-09-07 18:14:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `hari`
--

CREATE TABLE `hari` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode_hari` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `nama_hari` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `hari`
--

INSERT INTO `hari` (`id`, `kode_hari`, `nama_hari`, `created_at`, `updated_at`) VALUES
(1, 'Sun', 'Minggu', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(2, 'Mon', 'Senin', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(3, 'Tue', 'Selasa', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(4, 'Wed', 'Rabu', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(5, 'Thu', 'Kamis', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(6, 'Fri', 'Jumat', '2019-11-08 03:30:46', '2019-11-08 03:30:46'),
(7, 'Sat', 'Sabtu', '2019-11-08 03:30:46', '2019-11-08 03:30:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_praktik`
--

CREATE TABLE `jadwal_praktik` (
  `id` int(10) UNSIGNED NOT NULL,
  `hari` int(10) UNSIGNED DEFAULT NULL,
  `doctor` int(10) UNSIGNED DEFAULT NULL,
  `ruangan` int(10) UNSIGNED DEFAULT NULL,
  `mulai` time DEFAULT NULL,
  `selesai` time DEFAULT NULL,
  `ready` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `listruangan`
--

CREATE TABLE `listruangan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `listruangan`
--

INSERT INTO `listruangan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'R1', NULL, NULL),
(2, 'R2', NULL, NULL),
(3, 'R3', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `medicalrecord`
--

CREATE TABLE `medicalrecord` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diagnosa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `practice` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`practice`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `medicalrecords`
--

CREATE TABLE `medicalrecords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complaint` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diagnosa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `practice` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`practice`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `medicalrecords`
--

INSERT INTO `medicalrecords` (`id`, `name`, `complaint`, `diagnosa`, `note`, `patient_id`, `practice`, `created_at`, `updated_at`) VALUES
(1, 'rindu', 'hdhhd', 'hhhhh', 'jdjd', 1, '[\"tambal\"]', '2020-05-25 19:59:08', '2020-05-25 19:59:08'),
(2, 'rindu', 'hdhhd', 'hhhhh', 'jdjd', 1, '[\"tambal\"]', '2020-05-25 19:59:25', '2020-05-25 19:59:25'),
(3, 'rindu', 'hdhhd', 'sakit', 'goblok', 1, '[\"tambal\",\"operasi ginsul\"]', '2020-05-25 20:03:13', '2020-05-25 20:03:13'),
(4, 'rindu', 'hdhhd', 'sakit', 'ok', 1, '[\"tambal\",\"operasi ginsul\"]', '2020-05-25 20:06:49', '2020-05-25 20:06:49'),
(5, 'wilda', 'hati', 'sakit', 'ini', 2, '[\"tambal\"]', '2020-05-29 07:00:00', '2020-05-29 07:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(57, '2020_05_18_144222_create_register_table', 1),
(62, '2020_05_18_154130_create_serviceregister_table', 3),
(116, '2019_08_19_000000_create_failed_jobs_table', 4),
(117, '2020_05_12_174633_create_patients_table', 4),
(130, '2020_05_16_165249_create_auths_table', 5),
(131, '2020_05_16_170431_create_model_users_table', 5),
(132, '2020_05_18_173622_create_serviceregister_table', 5),
(133, '2020_05_20_143759_create_doctors_table', 5),
(134, '2020_05_22_154154_create_practices_table', 5),
(135, '2020_05_22_182054_create_medicalrecord_table', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `model_users`
--

CREATE TABLE `model_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `patients`
--

CREATE TABLE `patients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `place_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `religion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `citizenship` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identity_number` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `patients`
--

INSERT INTO `patients` (`id`, `name`, `place_of_birth`, `date_of_birth`, `gender`, `address`, `religion`, `job`, `citizenship`, `number`, `identity_number`, `created_at`, `updated_at`) VALUES
(1, 'rindu', 'bandung', '1999-10-01', 'wanita', 'jl gegerkalong bandung', 'islam', 'penganguran', 'indonesia', '0854748394', '1948493', '2020-05-25 15:02:55', '2020-05-25 15:02:55'),
(2, 'wilda', 'bandung', '1998-10-01', 'wanita', 'jln jakarta bandung', 'islam', 'judi', 'indonesia', '0843737464', '3203848392', '2020-05-25 15:03:42', '2020-05-25 15:03:42'),
(3, 'prabowo', 'bandung', '1966-02-03', 'PRIA', 'jln ciwaruga bandung', 'islam', 'TNI', 'INDONESIA', '085637773881', '3203209993849399', '2020-09-01 19:31:22', '2020-09-01 19:31:22'),
(4, 'gile', 'tangerang', '1997-03-01', 'PRIA', 'bandung', 'lain-lain', 'siswa', 'indonesia', '085555555', '9383833832', '2020-09-07 18:09:35', '2020-09-07 18:09:35'),
(5, 'dudi', 'tangerang', '1980-03-10', 'PRIA', 'bantern', 'islam', 'siswa', 'indonesia', '085111111111', '32032080880008', '2020-09-08 16:47:25', '2020-09-08 16:47:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `practices`
--

CREATE TABLE `practices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_practice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_doctor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `practices`
--

INSERT INTO `practices` (`id`, `name_practice`, `category_doctor`, `price`, `created_at`, `updated_at`) VALUES
(1, 'tambal gigi', 'umum', 6, '2020-09-01 13:33:43', '2020-09-01 14:00:24'),
(3, 'tambal gigi', 'Spesialis Konservasi Gigi (Sp. KG)', 8, '2020-09-01 14:03:46', '2020-09-01 14:03:59'),
(4, 'tambal laser', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', 9, '2020-09-02 01:47:00', '2020-09-02 01:47:00'),
(5, 'scalling', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', 10, '2020-09-03 20:20:46', '2020-09-03 20:20:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `price`
--

CREATE TABLE `price` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_practice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_doctor` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_update_harga` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `price`
--

INSERT INTO `price` (`id`, `name_practice`, `category_doctor`, `price`, `tanggal_update_harga`, `created_at`, `updated_at`) VALUES
(1, 'tambal gigi', 'umum', '100000', '2020-09-01 20:32:12', '2020-09-01 13:32:12', '2020-09-01 13:32:12'),
(2, 'tambal gigi', 'umum', '150000', '2020-09-01 20:33:43', '2020-09-01 13:33:43', '2020-09-01 13:33:43'),
(3, 'tambal gigi', 'spesialis', '200000', '2020-09-01 20:35:18', '2020-09-01 13:35:18', '2020-09-01 13:35:18'),
(4, 'tambal gigi', 'umum', '200000', '2020-09-01 20:55:38', '2020-09-01 13:55:38', '2020-09-01 13:55:38'),
(5, 'tambal gigi', 'umum', '250000', '2020-09-01 20:59:18', '2020-09-01 13:59:18', '2020-09-01 13:59:18'),
(6, 'tambal gigi', 'umum', '250000', '2020-09-01 21:00:24', '2020-09-01 14:00:24', '2020-09-01 14:00:24'),
(7, 'tambal gigi', 'spesialis', '500000', '2020-09-01 21:03:45', '2020-09-01 14:03:45', '2020-09-01 14:03:45'),
(8, 'tambal gigi', 'spesialis', '550000', '2020-09-01 21:03:59', '2020-09-01 14:03:59', '2020-09-01 14:03:59'),
(9, 'tambal laser', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', '200000', '2020-09-02 08:47:00', '2020-09-02 01:47:00', '2020-09-02 01:47:00'),
(10, 'scalling', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', '150000', '2020-09-04 03:20:46', '2020-09-03 20:20:46', '2020-09-03 20:20:46'),
(11, 'pemasangan kawat gigi', 'Spesialis Kedokteran Gigi Anak (Sp. KGA)', '500000', '2020-09-04 03:21:13', '2020-09-03 20:21:13', '2020-09-03 20:21:13'),
(12, 'tambal gigi laser', 'Spesialis Ortodonsia (Sp. Ort)', '200000', '2020-09-08 23:37:15', '2020-09-08 16:37:15', '2020-09-08 16:37:15'),
(13, 'tambal gigi laser', 'Spesialis Ortodonsia (Sp. Ort)', '250000', '2020-09-08 23:37:39', '2020-09-08 16:37:39', '2020-09-08 16:37:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `room_used`
--

CREATE TABLE `room_used` (
  `id` int(10) UNSIGNED NOT NULL,
  `ruangan` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hari` int(10) UNSIGNED DEFAULT NULL,
  `mulai` time DEFAULT NULL,
  `selesai` time DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `room_used`
--

INSERT INTO `room_used` (`id`, `ruangan`, `hari`, `mulai`, `selesai`, `created_at`, `updated_at`) VALUES
(30, 'R1', 1, '10:30:00', '12:00:00', '2020-09-13 04:39:20', '2020-09-13 04:39:20'),
(28, 'R1', 1, '08:00:00', '10:00:00', '2020-09-13 03:48:26', '2020-09-13 03:48:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE `ruangan` (
  `id` int(10) UNSIGNED NOT NULL,
  `doctor` int(10) UNSIGNED DEFAULT NULL,
  `ruangan` int(10) UNSIGNED DEFAULT NULL,
  `ready` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `ruangan`
--

INSERT INTO `ruangan` (`id`, `doctor`, `ruangan`, `ready`, `created_at`, `updated_at`) VALUES
(10, 4, 30, '0', '2020-09-13 04:39:20', '2020-09-13 04:39:20'),
(8, 2, 28, '0', '2020-09-13 03:48:26', '2020-09-13 03:48:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service_register`
--

CREATE TABLE `service_register` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_pendaftaran` int(100) NOT NULL,
  `complaint` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `id_doctor` bigint(20) UNSIGNED NOT NULL,
  `cek` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tanggalini`
--

CREATE TABLE `tanggalini` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `tanggalini`
--

INSERT INTO `tanggalini` (`id`, `tanggal`, `created_at`, `updated_at`) VALUES
(1, '2020-09-14', '2020-09-08 22:14:00', '2020-09-13 20:50:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_catatan_pemeriksaan` bigint(20) UNSIGNED NOT NULL,
  `id_karyawan` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date DEFAULT NULL,
  `nama_pasien` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_jenis_pelayanan` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_front_office` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori_transaksi` enum('pelayanan','produk','admin') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cara_pembayaran` enum('tunai','transfer(BCA)','DebitPermata','DebitBankLain','CCAllBank') COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_perawat` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `produk_tambahan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `id_catatan_pemeriksaan`, `id_karyawan`, `tanggal`, `nama_pasien`, `nama_jenis_pelayanan`, `harga`, `nama_front_office`, `kategori_transaksi`, `cara_pembayaran`, `nama_perawat`, `produk_tambahan`, `status`, `created_at`, `updated_at`) VALUES
(14, 1, 9, '2020-09-08', 'gile', '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"},\"6\":{\"name_practice\":\"pemasangan kawat gigi\"}}', '1050000', 'gilang', 'admin', 'transfer(BCA)', 'mei', '200000', '1', '2020-09-08 20:58:56', '2020-09-08 20:58:56'),
(15, 2, 9, '2020-09-08', 'dudi', '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', '350000', 'gilang', 'admin', 'CCAllBank', 'mei', '', '1', '2020-09-08 21:17:38', '2020-09-08 21:17:38'),
(16, 3, 9, '2020-09-08', 'wilda', '{\"4\":{\"name_practice\":\"tambal laser\"},\"6\":{\"name_practice\":\"pemasangan kawat gigi\"}}', '900000', 'gilang', 'admin', 'transfer(BCA)', 'mei', '200000', '1', '2020-09-08 21:20:05', '2020-09-08 21:20:05'),
(17, 4, 9, '2020-09-08', 'rindu', '{\"5\":{\"name_practice\":\"scalling\"}}', '150000', 'gilang', 'admin', 'transfer(BCA)', 'mei', '', '1', '2020-09-08 23:12:11', '2020-09-08 23:12:11'),
(18, 5, 9, '2020-09-09', 'prabowo', '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', '490000', 'gilang', 'admin', 'transfer(BCA)', 'mei', '140000', '1', '2020-09-09 05:37:18', '2020-09-09 05:37:18'),
(19, 6, 9, '2020-09-09', 'wilda', '{\"4\":{\"name_practice\":\"tambal laser\"},\"5\":{\"name_practice\":\"scalling\"}}', '400000', 'gilang', 'produk', 'tunai', 'mei', '50000', '1', '2020-09-09 09:23:37', '2020-09-09 09:23:37');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `auths`
--
ALTER TABLE `auths`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auths_username_unique` (`username`);

--
-- Indeks untuk tabel `catatan_pemeriksaan`
--
ALTER TABLE `catatan_pemeriksaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catatan_pemeriksaan_id_service_register_foreign` (`id_service_register`),
  ADD KEY `catatan_pemeriksaan_id_employee_foreign` (`id_employee`),
  ADD KEY `catatan_pemeriksaan_id_doctor_foreign` (`id_doctor`),
  ADD KEY `catatan_pemeriksaan_id_jenis_pelayanan_foreign` (`id_jenis_pelayanan`(768));

--
-- Indeks untuk tabel `category_doctor`
--
ALTER TABLE `category_doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `doctors_number_unique` (`number`),
  ADD UNIQUE KEY `doctors_identity_number_unique` (`identity_number`);

--
-- Indeks untuk tabel `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_number_unique` (`number`),
  ADD UNIQUE KEY `employees_identity_number_unique` (`identity_number`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `jadwal_praktik`
--
ALTER TABLE `jadwal_praktik`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jadwal_doctor_foreign` (`doctor`),
  ADD KEY `jadwal_hari_foreign` (`hari`),
  ADD KEY `jadwal_ruangan_foreign` (`ruangan`);

--
-- Indeks untuk tabel `listruangan`
--
ALTER TABLE `listruangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `medicalrecord`
--
ALTER TABLE `medicalrecord`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medicalrecord_patient_id_foreign` (`patient_id`);

--
-- Indeks untuk tabel `medicalrecords`
--
ALTER TABLE `medicalrecords`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medicalrecords_patient_id_foreign` (`patient_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `model_users`
--
ALTER TABLE `model_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `patients_number_unique` (`number`),
  ADD UNIQUE KEY `patients_identity_number_unique` (`identity_number`);

--
-- Indeks untuk tabel `practices`
--
ALTER TABLE `practices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `price_foreign` (`price`);

--
-- Indeks untuk tabel `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `room_used`
--
ALTER TABLE `room_used`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jadwal_doctor_foreign` (`doctor`),
  ADD KEY `jadwal_ruangan_foreign` (`ruangan`);

--
-- Indeks untuk tabel `service_register`
--
ALTER TABLE `service_register`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_register_patient_id_foreign` (`patient_id`),
  ADD KEY `catatan_pemeriksaan_id_doctor_foreign` (`id_doctor`);

--
-- Indeks untuk tabel `tanggalini`
--
ALTER TABLE `tanggalini`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksi_id_catata_pemeriksaan_foreign` (`id_catatan_pemeriksaan`),
  ADD KEY `transaksi_id_karyawan_foreign` (`id_karyawan`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `auths`
--
ALTER TABLE `auths`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `catatan_pemeriksaan`
--
ALTER TABLE `catatan_pemeriksaan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `category_doctor`
--
ALTER TABLE `category_doctor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `hari`
--
ALTER TABLE `hari`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `jadwal_praktik`
--
ALTER TABLE `jadwal_praktik`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `listruangan`
--
ALTER TABLE `listruangan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `medicalrecord`
--
ALTER TABLE `medicalrecord`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `medicalrecords`
--
ALTER TABLE `medicalrecords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT untuk tabel `model_users`
--
ALTER TABLE `model_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `patients`
--
ALTER TABLE `patients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `practices`
--
ALTER TABLE `practices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `price`
--
ALTER TABLE `price`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `room_used`
--
ALTER TABLE `room_used`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `service_register`
--
ALTER TABLE `service_register`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `tanggalini`
--
ALTER TABLE `tanggalini`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `medicalrecord`
--
ALTER TABLE `medicalrecord`
  ADD CONSTRAINT `medicalrecord_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `medicalrecords`
--
ALTER TABLE `medicalrecords`
  ADD CONSTRAINT `medicalrecords_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
