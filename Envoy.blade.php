@servers(['web-1' => 'root@37.44.244.184', 'web-2' => 'root@5.181.217.61', 'web-3' => 'root@5.181.217.177'])
@setup
    $repository = 'git@gitlab.com:yohanmartisa31/klinikgigi.git';
    $releases_dir = '/var/www/html/gigiku/releases_update';
    $app_dir = '/var/www/html/gigiku/cicd';    
    $release = '2';
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup
@story('deploy')
    clone_repository
    run_composer 
    update_symlinks  
@endstory

@story('rollback')
    rollback_symlinks    
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}    
    rm -rf {{ $new_release_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}   
    echo 'Done'
    chown -R apache.apache {{$new_release_dir}}
    chmod -R 755 {{$new_release_dir}}
@endtask
@task('run_composer')
    echo "Starting deployment"
    cd {{ $new_release_dir }}
    composer install --prefer-dist --no-scripts -q -o    
    ls
    echo 'ini oke'
    ls
    echo 'Done'
@endtask
@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage
    echo 'Done'
    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
    echo 'Done'
    echo 'Linking current release'        
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    echo '{{ $new_release_dir }}'
    cd {{ $new_release_dir }}
    php artisan migrate --force
    
@endtask
@task ('rollback_symlinks')    
    cd {{$releases_dir}}
    chown -R apache.apache {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1)
    chmod -R 755 {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1)     
    rm -rf {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1)/storage
    ln -nfs {{ $app_dir }}/storage {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1)/storage    
    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1)/.env
    ln -nfs {{ $releases_dir }}/$(find . -maxdepth 1 | sort  | tail -n 2 | head -n1) {{$app_dir}}/current
	cd {{$app_dir}}/current
    composer install --prefer-dist --no-scripts -q -o
@endtask